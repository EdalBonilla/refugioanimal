<!doctype html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
  <!-- Required meta tags -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <!-- Latest minified bootstrap js -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <!--  CSS -->
  <link rel="stylesheet" href="/css/style1.css">
  <link rel="stylesheet" href="/css/style2.css">
  <link rel="stylesheet" type="text/css" href="/css/donantes.css">
  <title>Salvadog</title>
</head>
<body>

  <nav class="navbar justify-content-center" style="background-color: #3d3d49;  ">
    <!-- Navbar content -->
    <ul class="nav justify-content-center" >
      <li class="nav-item" >
        <a class="nav-link active texoMenu" href="/">Salvadog</a>
      </li>
        <?php
            if(Auth::check()){
        ?>      
        <li class="nav-item">
        <a class="nav-link texoMenu" href="/adminEntidad"  >Perfil</a>
      </li>
     
      <li class="nav-item">
        <a class="nav-link texoMenu" href="/mascotas">Mascotas</a>
      </li>
      <li class="nav-item">
        <a class="nav-link texoMenu" href="/entidades">Usuarios</a>
      </li>
      
           <li class="nav-item">
        <a class="nav-link texoMenu" href="/categorias">Categorias de Animales</a>
      </li>
       <?php
            }
        ?>
      <li class="nav-item">
        <a class="nav-link texoMenu" href="/necesidades">Donaciones</a>
      </li>
      
        <?php
            if(Auth::check()){
        ?>  

      <li class="nav-item">
        <a class="nav-link texoMenu" href="/razas">Razas</a>
      </li>
 
    <li class="nav-item">
        <a class="nav-link texoMenu" href="/logout">Cerrar Sesión</a>
      </li>
      <?php
            }
        ?>
     </li>
   </ul>
 </nav>
 <div class="content container contenedor">
