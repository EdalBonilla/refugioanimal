<?php
include 'menu.php';
 ?>
<div class="row" style="    flex-wrap: wrap;
     margin-right: 0;
     margin-left: 0">
  <div class="col-md-4 text-center" id="cajadd1">

    <img src="img/user.PNG" id="avatar">

  </div>
  <div class="col-md-7 " id="cajadd2">
    <h5 class="text-center" id="tituloadd">Ingreso de nuevo Usuario</h5>
    <hr>
    <form class="needs-validation" novalidate>
      <div class="form-row">
        <div class="col-md-6 mb-3">
          <label for="validationCustom01">Nombres</label>
          <input type="text" class="form-control" id="validationCustom01" placeholder="Nombre de usuario" required>
          <div class="valid-feedback">
            Dato correcto!
          </div>
          <div class="invalid-feedback">
            Ingrese un Nombre de usuario
          </div>
        </div>
        <div class="col-md-6 mb-3">
          <label for="validationCustomUsername">Correo</label>
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text" id="inputGroupPrepend">@</span>
            </div>
            <input type="text" class="form-control" id="validationCustomUsername" placeholder="@email" aria-describedby="inputGroupPrepend" required>
            <div class="valid-feedback">
              Dato correcto!
            </div>
            <div class="invalid-feedback">
              Ingrese el correo
            </div>
          </div>
        </div>
      </div>
      <div class="form-row">
        <div class="col-md-6 mb-3">
          <label for="validationCustom03">Telefono</label>
          <input type="text" class="form-control" id="validationCustom03" placeholder="0000-0000" required>
          <div class="invalid-feedback">
            Ingrese un numero de Telefono
          </div>
        </div>
        <div class="col-md-6 mb-3">
          <label for="validationCustom04">Categoria </label>

          <select class="custom-select" required>
            <option value="">--------</option>
            <option value="1">One</option>
            <option value="2">Two</option>
            <option value="3">Three</option>
          </select>
          <div class="valid-feedback">
            Dato correcto
          </div>
          <div class="invalid-feedback">
            Ingrese un Nombre de usuario
          </div>
        </div>
        <div class="col-md-5 mb-3">
          <label for="validationCustom05">Contraseña</label>
          <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required>
          <div class="invalid-feedback">
            Ingrese una contraseña
          </div>
          <div class="valid-feedback">
            Dato correcto
          </div>
        </div><br>
        <div class="col-md-6 mb-3"></div>
        <div class="col-md-5 mb-3">
          <label for="validationCustom05">Confirmar contraseña</label>
          <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required>
          <div class="invalid-feedback">
            Ingrese la contraseña anterio
          </div>
          <div class="valid-feedback">
            Dato correcto
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="form-check">
          <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
          <label class="form-check-label" for="invalidCheck">
            Aceptar terminos y condiciones
          </label>
          <div class="invalid-feedback">
            Aceptar terminos y condiciones
          </div>
        </div>
      </div>
      <button class="btn btn-primary" type="submit">Agregar usuario</button>
    </form>


    <script>
      // Example starter JavaScript for disabling form submissions if there are invalid fields
      (function() {
        'use strict';
        window.addEventListener('load', function() {
          // Fetch all the forms we want to apply custom Bootstrap validation styles to
          var forms = document.getElementsByClassName('needs-validation');
          // Loop over them and prevent submission
          var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
              if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
              }
              form.classList.add('was-validated');
            }, false);
          });
        }, false);
      })();
    </script>
  </div>
</div>



<?php
include "footer.php";
 ?>
