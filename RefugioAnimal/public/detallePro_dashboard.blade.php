 @extends('public.main_template')

@section('title',  $producto->nombre)
@section('maintitle', $producto->nombre)
@section('li_active', 'productos')
@section('back_url', 'productos')
@section('back_txt', 'Regresar al listado de productos')
@section('content')
<br>
<div class="row">
	
<div class="col-md-4">
	<H4><strong>INFORMACIÓN</strong> <a href="producto_find_{{ $producto->codProducto }}" style="margin-left: 5px;" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i> MODIFICAR</a><a href="delete_producto_{{ $producto->codProducto }}" style="margin-left: 5px;" class="btn btn-xs btn-danger delete"><i class="fa fa-trash"></i> Eliminar</a></H4>
	<br>
	<style type="text/css">
		#info-table td {
			border-top: 0;
		}
	</style>
	<table id="info-table" class="table dt-responsive ">
		<tbody>
			<tr>
				<td><strong><big>CÓDIGO:</big></strong></td>
				<td><big> {{ $producto->codProducto }} </big></td>	
			</tr>
			<tr>
				<td><strong><big>PRODUCTO:</big></strong></td>
				<td><big> {{ $producto->nombre }} </big></td>
			</tr>
			<tr>
				<td><strong><big>CATEGORÍA:</big></strong></td>
				<td><big> {{ $producto->cat }} </big></td>
			</tr>
			<tr>
				<td><strong><big>EXISTENCIA TOTAL:</big></strong></td>
				<td><big> {{$total}} </big> </td>
			</tr>
			<tr">
				<td><strong><big>EXISTENCIA REQUISADA:</big></strong></td>
				<td><big> {{$trequisados}} </big> </td>
			</tr>
			<tr>
				<td><strong><big>EXISTENCIA DISPONIBLE:</big></strong></td>
				<td><big>{{$total - $trequisados}}  </big> </td>
			</tr>
		</tbody>
		<tfoot style="margin-top: 30px;">
			<tr>
				<td><strong><big>CREADO:</big></strong></td>
				<td><big> {{ $producto->created_at }} </big> </td>
			</tr>
			<tr>
				<td><strong><big>MODIFICADO:</big></strong></td>
				<td><big> {{ $producto->updated_at }} </big> </td>
			</tr>
		</tfoot>
			
	</table>
	  <br>
	  
</div>
<div class="col-md-8">
<H4><strong>EXISTENCIAS</strong> <a href="#"  data-toggle="modal" data-target="#add_detail" style="margin-left: 5px;" class="btn btn-xs btn-success"><i class="fa fa-plus"></i> REGISTRAR EXISTENCIA EN AREA</a> <a href="historial" style="margin-left: 5px;" class="btn btn-xs btn-warning"><i class="fa fa-history"></i> VER HISTORIAL DE ENTRADA/SALIDAS EN INVENTARIO</a></H4>
<br>
<table id="myTable" class="table table-striped  dt-responsive nowrap" cellspacing="0" width="100%">
	<thead>
    <tr>
      <th>#</th>
      <th>Area</th>
	  <th>Existencia</th>
      <th>Stock min</th>
      <th>estado</th>
      <th>Inventario</th>
	  <th>Acciones</th>
    </tr>
	</thead>
	<tbody>
	<?php $counter=1;  ?>
 	@foreach($detalles as $detalleProducto)
    	<tr>
    		<td>
    			{{ $counter }}
    		</td>
    		<td>
		    	{{ $detalleProducto->area }}
		    </td>
		    
		    <td>
		      	{{ $detalleProducto->existencia }}
		    </td>
		     <td>
		    	{{ $detalleProducto->minExistencia }}
		    </td>
		    <td class="estado">
		    	@if($detalleProducto->existencia < $detalleProducto->minExistencia)
		    		<span style="color:red; font-weight: bold;">Inestable</span>
		    	@endif
		    	@if($detalleProducto->existencia > $detalleProducto->minExistencia)
		    		<span style="color:blue; font-weight: bold;">Estable</span>
		    	@endif
		    	@if($detalleProducto->existencia == $detalleProducto->minExistencia)
		    		<span style="color:green; font-weight: bold;">Neutro</span>
		    	@endif
		    </td>
		   
		   <td>
		    	<a href="#"  data-toggle="modal" data-id="{{ $detalleProducto->codDetalle }}"  data-target="#stock" title="Añadir" class="btn btn-sm btn-success stock_add"><i class="fa fa-plus" aria-hidden="true"></i> ENTRADAS</a>
		    	<a href="#"  data-toggle="modal"  data-id="{{ $detalleProducto->codDetalle }}" data-target="#transferir" title="transferir"  class="btn btn-sm btn-warning transferir"><i class="fa fa-minus" aria-hidden="true"></i> TRANSFERIR</a>
		    	<a href="#"  data-toggle="modal"  data-id="{{ $detalleProducto->codDetalle }}" data-target="#stock" title="Quitar"  class="btn btn-sm btn-danger stock_delete"><i class="fa fa-minus" aria-hidden="true"></i> SALIDAS</a>
		    </td>

		    <td>
		    	<a href="#" data-toggle="modal" data-id="{{ $detalleProducto->codDetalle }}"  data-target="#edit_detail"  title="Editar" class="btn btn-sm btn-primary edit_detail"><i class="fa fa-pencil" aria-hidden="true"></i> EDITAR</a>
		    	<a href="delete_detallePro_{{$detalleProducto->codDetalle}}" title="Eliminar"  class="btn btn-sm btn-danger delete"><i class="fa fa-trash" aria-hidden="true"></i> ELIMINAR</a>
		    </td>

		      
		   
		    
		 
		    
	    </tr>
	    <?php $counter++; ?>
	@endforeach
</tbody>
</table>


<div id="add_detail" class="modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">AGREGAR EXISTENCIA EN AREA</h4>
      </div>
      <div class="modal-body">
      	@if(count($listArea) == 0)
      		El producto se encuentra registrado en todas las áreas disponibles.
      	@else
	      	<form action="insert_detallePro" method="post" class="form-horizontal " enctype="multipart/form-data">
	      	<input type="hidden" name="producto" value="{{ $producto->codProducto }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="form-group">
				<label class="col-md-4" for="enc" >Area</label>
					<div class="col-md-8">
	    				<select name="area" class="form-control">
	    					@foreach($listArea as $item)
	    							<option value="{{$item->codArea}}">{{$item->nombre}}</option>
	    					@endforeach
	    				</select>

						@if($errors->has('encargado'))
							<span class="label label-warning">{{$errors->first('area')}}</span>
						@endif 
					</div>
			</div> 
			<div class="form-group">
				<label class="col-md-4" for="cod" >Existencia mínima</label>
					<div class="col-md-8">
						<input type="text" class="form-control number_only" id="codPrducto" name="minExistencia" placeholder="Ingrese existencia mínima" value="{{ old('cod') }}">
						@if($errors->has('cod'))
							<span class="label label-warning">{{$errors->first('cod')}}</span>
						@endif 
					</div>
			</div>
		@endif
      </div>
      
      <div class="modal-footer">
      	@if(count($listArea) == 0)
      		<button type="button" class="btn btn-primary" data-dismiss="modal">CERRAR</button>
      	@else
	        <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
	        <button type="submit" class="btn btn-primary">GUARDAR CAMBIOS</button>
	        </form>
         @endif
      </div>
     
    </div>
  </div>
</div>

<div id="edit_detail" class="modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">{{ $producto->nombre }}</h4>
      </div>
      <div class="modal-body">
      	
	      	<form action="update_detallePro" method="post" class="form-horizontal " enctype="multipart/form-data">
	      	<input type="hidden" id="detalleProducto" name="detalleProducto" value="">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			
			<div class="form-group">
				<label class="col-md-4" for="cod" >Área</label>
					<div class="col-md-8">
						<input  type="text" class="area form-control" value="" disabled="">
						@if($errors->has('cod'))
							<span class="label label-warning">{{$errors->first('cod')}}</span>
						@endif 
					</div>
			</div>

			<div class="form-group">
				<label class="col-md-4" for="cod" >Existencia</label>
					<div class="col-md-8">
						<input type="text" class="exist form-control" value="" disabled="">
						@if($errors->has('cod'))
							<span class="label label-warning">{{$errors->first('cod')}}</span>
						@endif 
					</div>
			</div>

			<div class="form-group">
				<label class="col-md-4" for="cod" >Existencia mínima</label>
					<div class="col-md-8">
						<input type="text" class="minExist form-control number_only" name="minExistencia" placeholder="Ingrese existencia mínima" value="">
						@if($errors->has('cod'))
							<span class="label label-warning">{{$errors->first('cod')}}</span>
						@endif 
					</div>
			</div>

			<div class="form-group">
				<label class="col-md-4" for="cod" >Estado</label>
					<div class="col-md-8">
						<span class="estado"></span>
						@if($errors->has('cod'))
							<span class="label label-warning">{{$errors->first('cod')}}</span>
						@endif 
					</div>
			</div>

			<div class="form-group">
				<label class="col-md-4" for="cod" >Registro Creado: </label>
					<div class="col-md-8">
						<input type="text" class="created form-control" value="" disabled="">
						@if($errors->has('cod'))
							<span class="label label-warning">{{$errors->first('cod')}}</span>
						@endif 
					</div>
			</div>

			<div class="form-group">
				<label class="col-md-4" for="cod" >Última modificación</label>
					<div class="col-md-8">
						<input type="text" class="updated form-control" value="" disabled="">
						@if($errors->has('cod'))
							<span class="label label-warning">{{$errors->first('cod')}}</span>
						@endif 
					</div>
			</div>
			
		
      </div>
      
      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
	        <button type="submit" class="btn btn-primary">GUARDAR CAMBIOS</button>
      </div>
     </form>
    </div>
  </div>
</div>

<div id="stock" class="modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">{{ $producto->nombre }}</h4>
      </div>
      <div class="modal-body">
      	
	      	<form action="" method="post" class="form-horizontal " enctype="multipart/form-data">
	      	<input type="hidden" id="detalleProducto" name="detalleProducto" value="">
	      	
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			
			<div class="form-group">
				<label class="col-md-4" for="cod" >Área</label>
					<div class="col-md-8">
						<input  type="text" class="area form-control" value="" disabled="">
						@if($errors->has('cod'))
							<span class="label label-warning">{{$errors->first('cod')}}</span>
						@endif 
					</div>
			</div>

			<div class="form-group">
				<label class="col-md-4" for="cod" >Existencia actual</label>
					<div class="col-md-8">
						<input type="text" class="exist form-control" value="" disabled="">
						@if($errors->has('cod'))
							<span class="label label-warning">{{$errors->first('cod')}}</span>
						@endif 
					</div>
			</div>

			<div class="form-group">
				<label class="action-text col-md-4" for="cod"></label>
					<div class="col-md-8">
						<input type="text" class="current form-control number_only" name="cantidad" placeholder="Ingrese existencia mínima" value=""  autofocus>
						@if($errors->has('cod'))
							<span class="label label-warning">{{$errors->first('cod')}}</span>
						@endif 
					</div>
			</div>
					
			<div class="form-group">
				<label class="col-md-4" for="cod" >Existencia prevista</label>
					<div class="col-md-8">
						<input type="text" class="preview form-control" value="" disabled="">
						@if($errors->has('cod'))
							<span class="label label-warning">{{$errors->first('cod')}}</span>
						@endif 
					</div>
			</div>
      </div>
      
      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
	        <button type="submit" class="btn btn-primary" disabled="false">GUARDAR CAMBIOS</button>
      </div>
     </form>
    </div>
  </div>
</div>

<div id="transferir" class="modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">{{ $producto->nombre }}</h4>
      </div>
      <div class="modal-body">
      	
	      	<form action="" method="post" class="form-horizontal " enctype="multipart/form-data">
	      	<input type="hidden" id="detalleProducto" name="detalleProducto" value="">
	      	
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			
			<div class="form-group">
				<label class="col-md-4" for="cod" >Área</label>
					<div class="col-md-8">
						<input  type="text" class="area form-control" value="" disabled="">
						@if($errors->has('cod'))
							<span class="label label-warning">{{$errors->first('cod')}}</span>
						@endif 
					</div>

			</div>
			<div class="form-group">
				<label class="col-md-4" for="cod" >Existencia actual</label>
					<div class="col-md-8">
						<input type="text" class="exist form-control" value="" disabled="">
						@if($errors->has('cod'))
							<span class="label label-warning">{{$errors->first('cod')}}</span>
						@endif 
					</div>
			</div>
			<div class="form-group">
				<label class="col-md-4" for="enc" >Nueva area</label>
					<div class="col-md-8">
	    				<select id="select_area" name="area" class="form-control">
	    					@foreach($listArea_full as $item)
	    							<option value="{{$item->codArea}}">{{$item->nombre}}</option>
	    					@endforeach
	    				</select>

						@if($errors->has('encargado'))
							<span class="label label-warning">{{$errors->first('area')}}</span>
						@endif 
					</div>
			</div>
			<div class="form-group">
				<label class="action-text col-md-4" for="cod"></label>
					<div class="col-md-8">
						<input type="text" class="current_transfer form-control number_only" name="cantidad" placeholder="Ingrese existencia mínima" value=""  autofocus>
						@if($errors->has('cod'))
							<span class="label label-warning">{{$errors->first('cod')}}</span>
						@endif 
					</div>
			</div>

		
			
      </div>
      
      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
	        <button type="submit" class="btn btn-primary" disabled="false">GUARDAR CAMBIOS</button>
      </div>
     </form>
    </div>
  </div>
</div>

</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
        $('#myTable').DataTable();
     
		$(document).on("click", "a.delete", function(){
		if (confirm('¿Eliminar este registro?')) {
    		return true;
		} else {
		    return false;
		}
			
		});


		$('#myTable').on('click','.edit_detail',function () {
			var id = $(this).attr("data-id");
			$.ajax({
            type: "POST",
            url: 'get_product_ajax',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {id_detalle:id},
            success: function( data ) {

                $('#edit_detail .area').attr("value",data.area);
				$('#edit_detail #detalleProducto').attr("value",data.detalle);
				$('#edit_detail .exist').attr("value",data.existencia);
				$('#edit_detail .minExist').attr("value",data.minExistencia);
				if (data.existencia == data.minExistencia) {
					$('#edit_detail .estado').html("<span style='color:green; font-weight: bold;'>Neutro</span>");
				}else{
					if (data.existencia > data.minExistencia ) {
						$('#edit_detail .estado').html("<span style='color:blue; font-weight: bold;'>Estable</span>");
					}else{
						$('#edit_detail .estado').html("<span style='color:red; font-weight: bold;'>Inestable</span>");
					}
				}
				$('#edit_detail .created').attr("value",data.created_at);
				$('#edit_detail .updated').attr("value",data.updated_at);

				setTimeout(function (){
		        $('#edit_detail .current').focus();
		    }, 250);
            }});
		});
	

	$('#myTable').on('click','.stock_add',function () {
			var id = $(this).attr("data-id");
			$.ajax({
            type: "POST",
            url: 'get_product_ajax',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {id_detalle:id},
            success: function( data ) {

                $('#stock .modal-title').html("AGREGAR A INVENTARIO");
				$('#stock form').attr("action", "detallePro_add_stock");
				$('#stock .area').attr("value",data.area);
				$('#stock #detalleProducto').attr("value",data.detalle);
				$('#stock .exist').attr("value",data.existencia);
				$('#stock .preview').attr("value",data.existencia);
				$('#stock .current').val('');
				$('#stock .action-text').html("Cantidad a ingresar");
				$('#stock button[type=submit]').prop("disabled", true);

				setTimeout(function (){
		        $('#edit_detail .current').focus();
		    }, 250);
            }});
			
			

		});
	$('#myTable').on('click','.transferir',function () {
			var id = $(this).attr("data-id");
			$.ajax({
            type: "POST",
            url: 'get_product_ajax',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {id_detalle:id},
            success: function( data ) {

                $('#transferir .modal-title').html("TRANSFERIR INVENTARIO");
				$('#transferir form').attr("action", "detallePro_transfer_stock");
				$('#transferir .area').attr("value",data.area);

				$('#transferir #select_area').val(0);
				$('#transferir #select_area option').show();
				$('#transferir #select_area option[value="'+ data.codArea +'"]').hide();
				$('#transferir #detalleProducto').attr("value",data.detalle);
				$('#transferir .exist').attr("value",data.existencia);
				$('#transferir .preview').attr("value",data.existencia);
				$('#transferir .current_transfer').val('');
				$('#transferir .action-text').html("Cantidad a transferir");
				$('#transferir button[type=submit]').prop("disabled", true);

				setTimeout(function (){
		        $('#edit_detail .current').focus();
		    }, 250);
            }});
			
			

		});
	
	$('#myTable').on('click','.stock_delete',function () {
			var id = $(this).attr("data-id");
			$.ajax({
            type: "POST",
            url: 'get_product_ajax',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {id_detalle:id},
            success: function( data ) {

				$('#stock .modal-title').html("QUITAR DE INVENTARIO");
				$('#stock form').attr("action", "detallePro_delete_stock");
				$('#stock .area').attr("value",data.area);
				$('#stock #detalleProducto').attr("value",data.detalle);
				$('#stock .exist').attr("value",data.existencia);
				$('#stock .preview').attr("value",data.existencia);
				$('#stock .current').val('');
				$('#stock .action-text').html("Cantidad a quitar");
				$('#stock button[type=submit]').prop("disabled", true);

				setTimeout(function (){
		        $('#edit_detail .current').focus();
		    }, 250);
            }});
		});
	

		$('.number_only').bind('keyup paste', function(){
	        this.value = this.value.replace(/[^0-9]/g, '');
	  	});

		$(".current").on("keyup", function(){
			if ($(this).val() == "") {
				$('#stock button[type=submit]').prop("disabled", true);
				return;
			}

			var temp = Number($(this).val());
			var stock = Number($("#stock .exist").val());


			if ($("#stock form").attr("action") == "detallePro_add_stock") 
			{
				$("#stock .preview").attr("value",temp+stock);
			}
			else 
			{
				if (temp > stock ) {
					$("#stock .preview").attr("value","Dato no valido");
					$('#stock button[type=submit]').prop("disabled", true);
					return;
				}
				else
				{
					$("#stock .preview").attr("value",stock-temp);
				}
			}
			
			$('#stock button[type=submit]').prop("disabled", false);

		});		

		$(".current_transfer").on("keyup", function(){
			if ($(this).val() == "") {
				$('#transferir button[type=submit]').prop("disabled", true);
				return;
			}

			var temp = Number($(this).val());
			var stock = Number($("#transferir .exist").val());


			if (temp > stock ) {
				$("#transferir .preview").attr("value","Dato no valido");
				$('#transferir button[type=submit]').prop("disabled", true);
				return;
			}
			else
			{
				$("#transferir .preview").attr("value",stock-temp);
			}
			
			
			$('#transferir button[type=submit]').prop("disabled", false);

		});		
  	});


</script>




@endsection