<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
//Route::get('donaciones', "DonacionesController@index");
/*Route::get('/donaciones/list', function () {
    return view('donaciones');
})->name('donaciones.list');*/
/*
Route::get('/prueba', function () {
    return view('prueba');
})->name('prueba');
*/
//Ruta para mostrar tabla de entidades
//Route::get('/entidades', "EntidadesController@index");

Route::group(['middleware' => 'web'], function () {


//Mascota
Route::get('createMascota', "MascotaController@addForm");
Route::post('mascota_add', "MascotaController@create");
Route::get('mascotas', "MascotaController@index");
Route::get('adminMascota_{id}', "MascotaController@admin");
Route::get('deleteMascota_{id}', "MascotaController@delete");
Route::get('findMascota_{id}', "MascotaController@find");
Route::post('updateMascota', "MascotaController@update");
Route::post('gestionarMascota', "MascotaController@gestionMascota");
Route::get('mascota_lista', "MascotaController@lista");

//Necesidades
Route::get('necesidades', "NecesidadesController@index");


    Route::group(['middleware' => 'auth'], function () {
        //Entidades
        Route::get('entidades', "EntidadesController@index");
        Route::get('createEntidad', "EntidadesController@addForm");
        Route::post('entidad_add', "EntidadesController@create");
        Route::get('deleteEntidad_{id}', "EntidadesController@delete");
        Route::get('findEntidad_{id}', "EntidadesController@find");
        Route::post('updateEntidad', "EntidadesController@update");
        Route::get('adminEntidad_{id}', "EntidadesController@admin");
        Route::get('adminEntidad', "EntidadesController@adminSession");
        
        //Categorias
        Route::get('createCategoria', "CategoriaController@addForm");
        Route::post('categoria_add', "CategoriaController@create");
        Route::get('categorias', "CategoriaController@index");
        Route::get('deleteCategoria_{id}', "CategoriaController@delete");
        Route::get('findCategoria_{id}', "CategoriaController@find");
        Route::post('updateCategoria', "CategoriaController@update");
        
        //Raza
        Route::get('createRaza', "RazaController@addForm");
        Route::post('raza_add', "RazaController@create");
        Route::get('razas', "RazaController@index");
        Route::get('deleteRaza_{id}', "RazaController@delete");
        Route::get('findRaza_{id}', "RazaController@find");
        Route::post('updateRaza', "RazaController@update");
       
        //Donaciones
        Route::get('donaciones', "DonacionesController@index");
       
        //Necesidades
        Route::get('necesidades/crear', "NecesidadesController@addForm");
        Route::post('necesidades/crear', "NecesidadesController@create");
        Route::get('necesidades/necesidad/{id}', "NecesidadesController@find");
        
        //donativos
        Route::post('donaciones/donativo', "DonacionesController@create")->name("donaciones.donativo");
        
        //Formas de Pago
        Route::get('pagos/listar', "FormasPagoController@index");
        //Route::get('pagos/crear', "FormasPagoController@addForm");formas de pago no se prodra añadir por usuario
        //Route::post('pagos/crear', "FormasPagoController@create");formas de pago no se prodra añadir por usuario
        
        //Donables
        Route::get('donables', "DonablesController@index");
        Route::get('donables/donable/{id}', "DonablesController@edit");
        Route::get('donables/crear', "DonablesController@create");
        Route::post('donables/crear', "DonablesController@store");
        Route::post('donables/modificar', "DonablesController@update");
        Route::get('donables/eliminar/{id}', "DonablesController@delete");
    });
});


Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/logout', function(){
    Session::flush();
    Auth::logout();
    return Redirect::to("/")
      ->with('message', array('type' => 'success', 'text' => 'You have successfully logged out'));
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
