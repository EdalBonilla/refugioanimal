<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donaciones extends Model
{
    //
    protected $table = 'donaciones';

    protected $primaryKey= "idDonaciones";

    public function entidad(){
        return $this->belongsTo(Entidades::class,'idEntidad');
    }

    public function categoria(){
        return $this->belongsTo(Categoria::class,'idCategoria');
    }

    public function formaDePago(){
        return $this->belongsTo(FormasPago::class,'idformPago');
    }

    public function detalle(){
        return $this->hasMany(DonacionesDetalle::class,'idDonaciones');
    }
}
