<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Necesidades extends Model
{
    //
    protected $table = 'necesidades';

    protected $primaryKey= "idNecesidad";

   public function donable(){
        return $this->belongsTo(Donables::class,'idDonable');
    }

    public function donacionesDetalles(){
        return $this->hasMany(DonacionesDetalle::class,'idNecesidad');
    }
}
