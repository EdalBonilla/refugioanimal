<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormasPago extends Model
{
    //
    protected $table = 'formaspago';

    protected $primaryKey= "idFormaPago";

    public function donaciones(){
        return $this->hasMany(Donaciones ::class,'idformPago');
    }
}
