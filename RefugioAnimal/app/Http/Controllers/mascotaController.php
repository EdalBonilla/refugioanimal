<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mascota;
use App\DetalleMascota;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Session;

class mascotaController extends Controller
{
    
    //
    //redireccion formulario para agregar categorias
     public function index(Request $request)
    {
        $mascotas = DB::table('mascota')
            ->join('raza', 'mascota.idRaza','=','raza.idRaza')
            ->select('mascota.*', 'raza.nombre as raza')
            ->get();

        return  view('mascotas')->with( array( 'lista' => $mascotas));
    }

    public function lista(Request $request)
    {
        $mascotas = DB::table('mascota')
            ->join('raza', 'mascota.idRaza','=','raza.idRaza')
            ->select('mascota.*', 'raza.nombre as raza')
            ->get();

        return  view('mascota_lista')->with( array( 'lista' => $mascotas));
    }

    //redireccion formulario para agregar categorias
     public function addForm(Request $request)
    {
         $especies = DB::table('categoria')
            ->select('idCat','nombre')
            ->where('tipo', 'mascota_especie')
            ->get();
        $estados = DB::table('categoria')
            ->select('idCat','nombre')
            ->where('tipo', 'mascota_estado')
            ->get();

        $razas = DB::table('raza')
            ->select('idRaza','nombre')            
            ->get();
        return view("mascota_add")->with(array('estados'=>$estados, 'especies'=>$especies, 'razas'=>$razas ));
    }

    //agregando un nuevo registro
     public function create(Request $request)
    {
      	
		$nombre  =  $request->input("nombre");		
		$sexo = $request->input("sexo"); 
		$edad = $request->input("edad");
		$fechaIngreso  =  $request->input("fecha");
		$estado  =  $request->input("estado");
		$raza = $request->input("raza"); 
		$condicion = $request->input("condicion");
		$esterelizado  =  $request->input("est");
		$especie  =  $request->input("especie");
        $foto = "";
        

        

        if ($_FILES != "") {
            # code...
             $files = $request->file('imgFile');             
            $foto=$_FILES["imgFile"]["name"];
            //$fileRoute=$_FILES["imgFile"]["tmp_name"];
            //move_uploaded_file($fileRoute,  public_path('images').$fileName);
            $files->move(public_path('images'), $files->getClientOriginalName());
        }else{
            $fileName='default.jpg';
            $foto = $fileName;
        }
		
		

		$rules = [
            'nombre' => 'required|unique:mascota',            
            'edad'  => 'required',
            'fecha'  => 'required',
            'estado'  => 'required',
            'raza'  => 'required',
            'condicion'  => 'required',
            'est'  => 'required',
            'especie'  => 'required',

        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return  Redirect::to('createMascota')->withErrors($validator)->withInput($request->all()); //si hay error redirecciona al formulario             
        }

        $myObject = new Mascota();
        $myObject->nombre = $nombre;        
        $myObject->foto = $foto;
        $myObject->sexo = $sexo;
        $myObject->edad = $edad;
        $myObject->fechaIngreso = $fechaIngreso;
        $myObject->estado = $estado;
        $myObject->idRaza = $raza;
        $myObject->condicion = $condicion;
        $myObject->esterelizado = $esterelizado;
        $myObject->especie = $especie;
        
        
        try {
        	if ($myObject->save()) {
        		# code...
        		return  Redirect::to('mascotas'); //redirige a vista donde se muestra la lista de los registros

        	}else{
        		Session::flash('messageErr', "**". $e->getMessage());
            	return  Redirect::to('createMascota')->withErrors($validator)->withInput($request->all());//si hay error redirecciona al formulario
        	}
        	
        } catch (Exception $e) {
        	Session::flash('messageErr', "**". $e->getMessage());
            return  Redirect::to('createMascota')->withErrors($validator)->withInput($request->all()); //si hay error redirecciona al formulario
        }
    }

    //Busqueda del registro que se modificara
    public function find(Request $request, $id){
        
        $myObject = DB::table('mascota')
            ->where('idMascota',$id)
             ->join('raza', 'mascota.idRaza','=','raza.idRaza')
            ->select('mascota.*', 'raza.nombre as raza')
            ->get();
        $razas = DB::table('raza')
            ->select('idRaza','nombre')            
            ->get();

        if ($myObject != null) {
            return  view('mascota_mod')->with(array('mascota'=> $myObject[0], 'razas'=> $razas)); //retorna el registro encontrado a las vista del formularoi para modificar
        }else{
            Session::flash('messageErr', "La Categoria no existe o fue eliminada");
            return  Redirect::to('mascotas'); //Si no encuentra el objeto regresa a la vista donde se listan todos los objetos
        }
    }
    //Modificando el registro
    public function update(Request $request )
    {
        $idMascota = $request->input("id");

        $myObject = Mascota::find($request->input("id"));

        if ($myObject == null) {
            return  Redirect::to("mascotas"); //si no se encuentra regresa a la lista
        }

        $nombre  =  $request->input("nombre");		
		$sexo = $request->input("sexo"); 
		$edad = $request->input("edad");
		$fechaIngreso  =  $request->input("fecha");		
		$raza = $request->input("raza"); 
		$condicion = $request->input("condicion");
		$esterelizado  =  $request->input("est");
		$especie  =  $request->input("especie");
		
		

		$rules = [
            'nombre' => 'required',            
            'edad'  => 'required',
            'fecha'  => 'required',            
            'raza'  => 'required',
            'condicion'  => 'required',
            'est'  => 'required',
            'especie'  => 'required',

        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return  Redirect::to('createMascota')->withErrors($validator)->withInput($request->all()); //si hay error redirecciona al formulario             
        }
        
        $myObject->nombre = $nombre;        
        $myObject->sexo = $sexo;
        $myObject->edad = $edad;
        $myObject->fechaIngreso = $fechaIngreso;        
        $myObject->idRaza = $raza;
        $myObject->condicion = $condicion;
        $myObject->esterelizado = $esterelizado;
        $myObject->especie = $especie;

        try {
            if ($myObject->save()) {
                # code...
                return  Redirect::to('mascotas');//regresa al listado 
            }else{
                Session::flash('messageErr', "**". $e->getMessage());
                return  Redirect::to('findMascota_'.$id )->withErrors($validator)->withInput($request->all());//si hay error redirecciona al formulario 
            }
            
        } catch (Exception $e) {
            Session::flash('messageErr', "**". $e->getMessage());
            return  Redirect::to('findMascota_'.$id)->withErrors($validator)->withInput($request->all()); //si hay error redirecciona al formulario 
        }

        return Redirect::to("mascotas");//regresa al listado 

    }

    public function delete(Request $request, $id)
    {
        $myObject= Mascota::find($id);

        if ($myObject == null) {
            Session::flash('messageErr', "Error while deleting area");
            return Redirect::to("mascotas"); 
        }
        try{
            if($myObject->delete()){
                Session::flash('messageOk', "Mascota eliminada exitosamente");
                return  Redirect::to('mascotas');
            }else{
                Session::flash('messageErr', "Error eliminando la Mascota");
                return Redirect::to("mascotas"); 
            }
        } catch (Exception $e) {
            Session::flash('messageErr', "**". $e->getMessage());
            return Redirect::to("mascotas"); 
        }
    }

    public function admin(Request $request, $id){
       $myObject = DB::table('mascota')
            ->where('idMascota',$id)
             ->join('raza', 'mascota.idRaza','=','raza.idRaza')
            ->select('mascota.*', 'raza.nombre as raza')
            ->get();

         $categorias = DB::table('categoria')
            ->select('idCat','nombre')           
            ->get();

        if ($myObject != null) {
            return  view('mascota_admin')->with(array('mascota'=> $myObject[0], 'categorias'=>$categorias)); //retorna el registro encontrado a las vista del formularoi para modificar
        }else{
            Session::flash('messageErr', "La Categoria no existe o fue eliminada");
            return  Redirect::to('mascotas'); //Si no encuentra el objeto regresa a la vista donde se listan todos los objetos
        }

    }

    public function gestionMascota(Request $request){
        $value=$request->user();
        
        
        
        $idMascota  =  $request->input('idMascota');    
        $idEntidades = $value['id']; 
        $descripcion = "En proceso de adopcion";
        $tipo  =  $request->input('opcion');
        
        
        
         try {
           DB::select('call gestionMascota(?,?,?,?)', array($idMascota, $idEntidades, $descripcion, $tipo));
            # code...
            return  Redirect::to('adminMascota_'.$idMascota); //redirige a vista donde se muestra la lista de los registros

            
        } catch (Exception $e) {
            Session::flash('messageErr', "**". $e->getMessage());
            return  Redirect::to('adminMascota_'.$idMascota)->withErrors($validator)->withInput($request->all()); //si hay error redirecciona al formulario
        }

    }



}
