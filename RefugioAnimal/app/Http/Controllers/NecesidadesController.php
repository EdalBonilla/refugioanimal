<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\NecesidadesCreateRequest;
use App\Necesidades;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

//use Illuminate\Support\Facades\Validator;
use session;
use App\FormasPago;
use App\Donables;
use App\DonacionesDetalle;

class NecesidadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $necesidades = Necesidades::All();
        return  view('necesidades')->with('lista', $necesidades);
    }

      //*******redireccion formulario para agregar categorias****************************************
     public function addForm(Request $request)
    {
        $donables= Donables::all();
        return view("necesidades_add")->with('donables',$donables);
    }





    //**************************************************************************
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(NecesidadesCreateRequest $request)
    {
        //
        $titulo = $request->input("titulo");
        $cantidad = $request->input("cantidad");
        $descripcion = $request->input("descripcion");
        $idDonable = $request->input("donable");
        $fechaFin = $request->input("fecha_limite");
        $estado = 1; //siempre como activa
        $idMotivo = 1;
        $idBeneficiado = 1;

        $necesidad = new Necesidades();
        $necesidad->titulo = $titulo;
        $necesidad->cantidad = $cantidad;
        $necesidad->descripcion = $descripcion;
        $necesidad->idDonable = $idDonable;
        $necesidad->fechaFin = $fechaFin;
        $necesidad->estado = $estado;
        $necesidad->idMotivo = $idMotivo;
        $necesidad->idBeneficiado = $idBeneficiado;

        try {
            if ($necesidad->save()) {
                # code...
                return  Redirect::to('necesidades'); //redirige a vista donde se muestra la lista de los registros

            } else {
                $request->session()->flash('messageErr', "**");
                return  Redirect::to('necesidades_add'); //->withErrors($validator)->withInput($request->all());//si hay error redirecciona al formulario
            }
        } catch (\Exception $e) {
            $request->session()->flash('messageErr', "**" . $e->getMessage());
            return  Redirect::to('necesidades_add'); //->withErrors($validator)->withInput($request->all()); //si hay error redirecciona al formulario
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /*
    public function store(Request $request)
    {
        //
        $request->validate([
            'nombre' => 'required|max:50',
            'email' => 'required|max:255',
              'telefono' => 'required|max:50',
                'categoria' => 'required|max:50',
                  'password' => 'required|max:50',
                    'estado' => 'required|max:50',
        ]);

        //Utilizando Eloquent para insertar dentro de la base de datos
        $entidad=new Entidades;
        $entidad->nombre = $request->nombre;
        $entidad->email = $request->email;
         $entidad->telefono = $request->telefono;
          $entidad->categoria = $request->categoria;
           $entidad->password = $request->password;
            $entidad->estado = $request->estado;

        $entidad->save();

        return $this->index();


    }*/

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  /*  public function edit(Request $request, $id)
    {
        //
        $entidades = DB::table('entidades')
                    ->where('idEntidad', $id)
                    ->select('entidades.*')
                    ->get();
        return view('entidadesedit')->with(array('entidades'=>$entidades[0]));       

    }*/

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
     $idEntidad = $request->input("id");
     $necesidad = Entidades::find($request->input("id"));

     if ($necesidad == null) {
         # code...
        return Redirect::to("indexEntidad");
     }

     $nombre = $request->input("nombre");

     $email = $request->input("email");

     $telefono = $request->input("telefono");

     $categoria = $request->input("categoria");

     $password = $request->input("password");

     $estado = $request->input("estado");

 
  $rules= [
            'nombre' => 'request',
            'email' => 'request',
               'telefono' => 'request',
                  'categoria' => 'request',
                     'password' => 'request',
                        'estado' => 'request',

        ];

         try {
            if ($necesidad->save()) {
                # code...
                return  Redirect::to('indexEntidad');//regresa al listado 
            }else{
                Session::flash('messageErr', "**". $e->getMessage());
                return  Redirect::to('findEntidad_'.$id )->withErrors($validator)->withInput($request->all());//si hay error redirecciona al formulario 
            }
            
        } catch (Exception $e) {
            Session::flash('messageErr', "**". $e->getMessage());
            return  Redirect::to('findEntidad_'.$id)->withErrors($validator)->withInput($request->all()); //si hay error redirecciona al formulario 
        }

        return Redirect::to("indexEntidad");//regresa al listado 



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        //
     $necesidad= Entidades::find($id);

        if ($necesidad == null) {
            Session::flash('messageErr', "Error while deleting area");
            return Redirect::to("indexEntidad"); 
        }
        try{
            if($necesidad->delete()){
                
                return  Redirect::to('indexEntidad');
            }else{
                Session::flash('messageErr', "Error eliminando el Categoria");
                return Redirect::to("indexEntidad"); 
            }
        } catch (Exception $e) {
            Session::flash('messageErr', "**". $e->getMessage());
            return Redirect::to("indexEntidad"); 
        }


    }


    // Metodod OK 
    public function find(Request $request, $id)
    {
        $necesidad = DB::table('necesidades')
            ->where('idNecesidad', $id)
            ->select('necesidades.*')
            ->get();

        $formasDePago = FormasPago::all()->where('estado', 1);
        
        $donacionesRealizadas = DonacionesDetalle::all()->where('idNecesidad',$id);
        $totalDonaciones=0.00;
        if($donacionesRealizadas!=null){  
            foreach($donacionesRealizadas as $detalle){
                $totalDonaciones+=$detalle->cantidad;
            }
        }

        if ($necesidad != null) {
            return  view('donaciones_donativo')->with('necesidad', $necesidad[0])->with('formasDePago', $formasDePago)->with('donacionesRealizadas',$donacionesRealizadas)->with('totalDonaciones',$totalDonaciones); //retorna el registro encontrado a las vista del formularoi para modificar
        } else {
            Session::flash('messageErr', "La Categoria no existe o fue eliminada");
            return  Redirect::to('necesidades'); //Si no encuentra el objeto regresa a la vista donde se listan todos los objetos
        }
    }
}
