<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entidades;
use App\Mascota;
use App\DetalleMascota;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;

//use Illuminate\Support\Facades\Validator;
use session;

class EntidadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    { 
        /*
        $entidades = Entidades::all();   
        return view('entidades')->with('entidades',$entidades);*/

    $entidad = DB::table('entidades')
            ->select('entidades.*')
            ->get();

        return  view('entidades')->with( array( 'lista' => $entidad));

    }

      //*******redireccion formulario para agregar categorias****************************************
     public function addForm(Request $request)
    {
        return view("entidades_add");
    }

//**************************************************************************
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $nombre = $request->input("nombre");
        $email = $request->input("email");
        $telefono = $request->input("telefono");
        $categoria = $request->input("categoria");
        $password = $request->input("password");
        $estado = $request->input("estado");


        $rules= [
            'nombre' => 'request',
            'email' => 'request',
               'telefono' => 'nullable',
                  'categoria' => 'request',
                     'password' => 'request',
                        'estado' => 'request',

        ];

    /*    $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return  Redirect::to('createEntidad')->withErrors($validator)->withInput($request->all()); //si hay error redirecciona al formulario             
        }*/

        $myobject = new Entidades();
        $myobject->nombre = $nombre;
        $myobject->email = $email;
        $myobject->telefono = $telefono;
        $myobject->categoria = $categoria;
        $myobject->password = $password;
        $myobject->estado = $estado;


        try {
            if ($myobject->save()) {
                # code...
                return  Redirect::to('entidades'); //redirige a vista donde se muestra la lista de los registros

            }else{
                Session::flash('messageErr', "**". $e->getMessage());
                return  Redirect::to('createEntidad')->withErrors($validator)->withInput($request->all());//si hay error redirecciona al formulario
            }
            
        } catch (Exception $e) {
            Session::flash('messageErr', "**". $e->getMessage());
            return  Redirect::to('createEntidad')->withErrors($validator)->withInput($request->all()); //si hay error redirecciona al formulario
        }


     
    }

    public function update(Request $request)
    {
     $idEntidad = $request->input("id");
     $myobject = Entidades::find($request->input("id"));

     if ($myobject == null) {
         # code...
        return Redirect::to("entidades");
     }

     $nombre = $request->input("nombre");
     $email = $request->input("email");
     $telefono = $request->input("telefono");

 
      $rules= [
                'nombre' => 'request',
                'email' => 'request',
                   'telefono' => 'nullable',                      
            ];


        $myobject->nombre = $nombre;
        $myobject->email = $email;
        $myobject->telefono = $telefono;
        

         try {
            if ($myobject->save()) {
                # code...
                return  Redirect::to('entidades');//regresa al listado 
            }else{
                Session::flash('messageErr', "**". $e->getMessage());
                return  Redirect::to('findEntidad_'.$id )->withErrors($validator)->withInput($request->all());//si hay error redirecciona al formulario 
            }
            
        } catch (Exception $e) {
            Session::flash('messageErr', "**". $e->getMessage());
            return  Redirect::to('findEntidad_'.$id)->withErrors($validator)->withInput($request->all()); //si hay error redirecciona al formulario 
        }

        return Redirect::to("entidades");//regresa al listado 



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        //
     $myobject= Entidades::find($id);

        if ($myobject == null) {
            Session::flash('messageErr', "Error while deleting area");
            return Redirect::to("entidades"); 
        }
        try{
            if($myobject->delete()){
                
                return  Redirect::to('entidades');
            }else{
                Session::flash('messageErr', "Error eliminando el Categoria");
                return Redirect::to("entidades"); 
            }
        } catch (Exception $e) {
            Session::flash('messageErr', "**". $e->getMessage());
            return Redirect::to("entidades"); 
        }


    }



public function find(Request $request, $id){


 $myObject = DB::table('entidades')
            ->where('idEntidad',$id)
            ->select('entidades.*')
            ->get();

        if ($myObject != null) {
            return  view('entidadesedit')->with(array('entidades'=> $myObject[0])); //retorna el registro encontrado a las vista del formularoi para modificar
        }else{
            Session::flash('messageErr', "La Categoria no existe o fue eliminada");
            return  Redirect::to('entidades'); //Si no encuentra el objeto regresa a la vista donde se listan todos los objetos
        }

    }

public function admin(Request $request, $id){


 $myObject = DB::table('entidades')
            ->where('idUsers',$id)
            ->select('entidades.*')
            ->get();

        if ($myObject != null) {
            return  view('entidades_admin')->with(array('entidades'=> $myObject[0])); //retorna el registro encontrado a las vista del formularoi para modificar
        }else{
            Session::flash('messageErr', "La Categoria no existe o fue eliminada");
            return  Redirect::to('entidades'); //Si no encuentra el objeto regresa a la vista donde se listan todos los objetos
        }

    }


public function adminSession(Request $request){

$value=$request->user();


 $myObject = DB::table('entidades')
            ->leftJoin('detallemascota','entidades.idEntidad', '=', 'detallemascota.idEntidades')
            ->where('idUsers', $value['id'])
            ->select('entidades.*')
            ->get();
            

        if ($myObject != null) {
            return  view('entidades_admin')->with(array('entidades'=> $myObject[0])); //retorna el registro encontrado a las vista del formularoi para modificar
        }else{
            Session::flash('messageErr', "La Categoria no existe o fue eliminada");
            return  Redirect::to('entidades'); //Si no encuentra el objeto regresa a la vista donde se listan todos los objetos
        }

    }

}
