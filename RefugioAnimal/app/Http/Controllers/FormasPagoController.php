<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FormasPago;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

//use Illuminate\Support\Facades\Validator;
use session;
use Dotenv\Regex\Result;

class FormasPagoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formasDePago = FormasPago::All();
        return  view('formas_pago')->with('lista', $formasDePago);
    }

    //este metodo es create**
      //*******redireccion formulario para agregar categorias****************************************
     public function addForm(Request $request)
    {
        return view("formaspago_add");
    }





//**************************************************************************
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // Metodod OK 
    public function create(Request $request)
    {
        //recuperando parametros del formulario en la petición
        $nombre = $request->input("nombre");
        $icono = $request->input("icono");
        $estado = $request->input("estado");
        $detalles = $request->input("otros_detalles");

        $rules = [
            'nombre' => 'request',
            'email' => 'request',
            'telefono' => 'request',
            'categoria' => 'request',
            'password' => 'request',
            'estado' => 'request',

        ];

        /*    $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return  Redirect::to('createEntidad')->withErrors($validator)->withInput($request->all()); //si hay error redirecciona al formulario             
        }*/

        $formaDePago = new FormasPago();
        $formaDePago->nombre = $nombre;
        $formaDePago->icono = $icono;
        $formaDePago->estado = $estado;
        $formaDePago->otros_detalle = $detalles;

        try {
            DB::beginTransaction();
            $formaDePago->save();
            $detalleDonacion->idDonaciones = $formaDePago->idDonaciones;
            $detalleDonacion->save();
            DB::commit();
            //enviar la transaccion al wspg
            return  Redirect::to('donaciones');
        } catch (\Exception $e) {
            DB::rollBack();
            $request->session()->flash('messageErr', $e->getMessage());
            return  Redirect::to('donaciones'); //->withErrors($validator)->withInput($request->all()); //si hay error redirecciona al formulario
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /*
    public function store(Request $request)
    {
        //
        $request->validate([
            'nombre' => 'required|max:50',
            'email' => 'required|max:255',
              'telefono' => 'required|max:50',
                'categoria' => 'required|max:50',
                  'password' => 'required|max:50',
                    'estado' => 'required|max:50',
        ]);

        //Utilizando Eloquent para insertar dentro de la base de datos
        $entidad=new Entidades;
        $entidad->nombre = $request->nombre;
        $entidad->email = $request->email;
         $entidad->telefono = $request->telefono;
          $entidad->categoria = $request->categoria;
           $entidad->password = $request->password;
            $entidad->estado = $request->estado;

        $entidad->save();

        return $this->index();


    }*/

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  /*  public function edit(Request $request, $id)
    {
        //
        $entidades = DB::table('entidades')
                    ->where('idEntidad', $id)
                    ->select('entidades.*')
                    ->get();
        return view('entidadesedit')->with(array('entidades'=>$entidades[0]));       

    }*/

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
     $idEntidad = $request->input("id");
     $formaDePago = Entidades::find($request->input("id"));

     if ($donacion == null) {
         # code...
        return Redirect::to("indexEntidad");
     }

     $nombre = $request->input("nombre");

     $email = $request->input("email");

     $telefono = $request->input("telefono");

     $categoria = $request->input("categoria");

     $password = $request->input("password");

     $estado = $request->input("estado");

 
  $rules= [
            'nombre' => 'request',
            'email' => 'request',
               'telefono' => 'request',
                  'categoria' => 'request',
                     'password' => 'request',
                        'estado' => 'request',

        ];

         try {
            if ($donacion->save()) {
                # code...
                return  Redirect::to('indexEntidad');//regresa al listado 
            }else{
                Session::flash('messageErr', "**". $e->getMessage());
                return  Redirect::to('findEntidad_'.$id )->withErrors($validator)->withInput($request->all());//si hay error redirecciona al formulario 
            }
            
        } catch (Exception $e) {
            Session::flash('messageErr', "**". $e->getMessage());
            return  Redirect::to('findEntidad_'.$id)->withErrors($validator)->withInput($request->all()); //si hay error redirecciona al formulario 
        }

        return Redirect::to("indexEntidad");//regresa al listado 



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        //
     $formaDePago= Entidades::find($id);

        if ($donacion == null) {
            Session::flash('messageErr', "Error while deleting area");
            return Redirect::to("indexEntidad"); 
        }
        try{
            if($donacion->delete()){
                
                return  Redirect::to('indexEntidad');
            }else{
                Session::flash('messageErr', "Error eliminando el Categoria");
                return Redirect::to("indexEntidad"); 
            }
        } catch (Exception $e) {
            Session::flash('messageErr', "**". $e->getMessage());
            return Redirect::to("indexEntidad"); 
        }


    }



    public function find(Request $request, $id){


 $formaDePago = DB::table('entidades')
            ->where('idEntidad',$id)
            ->select('entidades.*')
            ->get();

        if ($donacion != null) {
            return  view('entidadesedit')->with(array('entidades'=> $formaDePago[0])); //retorna el registro encontrado a las vista del formularoi para modificar
        }else{
            Session::flash('messageErr', "La Categoria no existe o fue eliminada");
            return  Redirect::to('indexEntidad'); //Si no encuentra el objeto regresa a la vista donde se listan todos los objetos
        }




    }
}
