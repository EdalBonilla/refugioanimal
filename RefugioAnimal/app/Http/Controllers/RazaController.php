<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Raza;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Session;

class RazaController extends Controller
{
    //
    //redireccion formulario para agregar categorias
     public function index(Request $request)
    {
        $raza = DB::table('raza')
            ->select('raza.*')
            ->get();

        return  view('razas')->with( array( 'lista' => $raza));
    }

    //redireccion formulario para agregar categorias
     public function addForm(Request $request)
    {
        return view("raza_add");
    }

    //agregando un nuevo registro
     public function create(Request $request)
    {
      	
		$nombre  =  $request->input("nombre");
		$descripcion = $request->input("desc"); 


		$rules = [
            'nombre' => 'required|unique:categoria',
            'desc'  => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return  Redirect::to('createRaza')->withErrors($validator)->withInput($request->all()); //si hay error redirecciona al formulario             
        }

        $myObject = new Raza();
        $myObject->nombre = $nombre;
        $myObject->descripcion = $descripcion;
        

        try {
        	if ($myObject->save()) {
        		# code...
        		return  Redirect::to('indexRaza'); //redirige a vista donde se muestra la lista de los registros

        	}else{
        		Session::flash('messageErr', "**". $e->getMessage());
            	return  Redirect::to('createRaza')->withErrors($validator)->withInput($request->all());//si hay error redirecciona al formulario
        	}
        	
        } catch (Exception $e) {
        	Session::flash('messageErr', "**". $e->getMessage());
            return  Redirect::to('createRaza')->withErrors($validator)->withInput($request->all()); //si hay error redirecciona al formulario
        }
    }
    //Busqueda del registro que se modificara
    public function find(Request $request, $id){
        
        $myObject = DB::table('raza')
            ->where('idRaza',$id)
            ->select('raza.*')
            ->get();

        if ($myObject != null) {
            return  view('raza_mod')->with(array('raza'=> $myObject[0])); //retorna el registro encontrado a las vista del formularoi para modificar
        }else{
            Session::flash('messageErr', "La Categoria no existe o fue eliminada");
            return  Redirect::to('indexRaza'); //Si no encuentra el objeto regresa a la vista donde se listan todos los objetos
        }
    }
    //Modificando el registro
    public function update(Request $request )
    {
        $idRaza = $request->input("id");

        $myObject = Raza::find($request->input("id"));

        if ($myObject == null) {
            return  Redirect::to("indexRaza"); //si no se encuentra regresa a la lista
        }

        $nombre  =  $request->input("nombre");
		$descripcion = $request->input("desc"); 
		

		$rules = [
            'nombre' => 'required',
            'desc'  => 'required',
           
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return  Redirect::to('createRaza')->withErrors($validator)->withInput($request->all()); //si hay error redirecciona al formulario             
        }
        
        $myObject->nombre = $nombre;
        $myObject->descripcion = $descripcion;
        

        try {
            if ($myObject->save()) {
                # code...
                return  Redirect::to('indexRaza');//regresa al listado 
            }else{
                Session::flash('messageErr', "**". $e->getMessage());
                return  Redirect::to('findRaza_'.$id )->withErrors($validator)->withInput($request->all());//si hay error redirecciona al formulario 
            }
            
        } catch (Exception $e) {
            Session::flash('messageErr', "**". $e->getMessage());
            return  Redirect::to('findRaza__'.$id)->withErrors($validator)->withInput($request->all()); //si hay error redirecciona al formulario 
        }

        return Redirect::to("indexRaza");//regresa al listado 

    }

    public function delete(Request $request, $id)
    {
        $myObject= Raza::find($id);

        if ($myObject == null) {
            Session::flash('messageErr', "Error while deleting area");
            return Redirect::to("indexRaza"); 
        }
        try{
            if($myObject->delete()){
                Session::flash('messageOk', "Categoria eliminada exitosamente");
                return  Redirect::to('indexRaza');
            }else{
                Session::flash('messageErr', "Error eliminando el Categoria");
                return Redirect::to("indexRaza"); 
            }
        } catch (Exception $e) {
            Session::flash('messageErr', "**". $e->getMessage());
            return Redirect::to("indexRaza"); 
        }
    }
}
