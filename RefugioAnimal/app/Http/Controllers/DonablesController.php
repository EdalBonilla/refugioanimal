<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\DonablesCreateRequest;
use App\Http\Requests\DonablesUpdateRequest;
use App\Http\Requests\DonablesDeleteRequest;
use App\Donaciones;
use App\DonacionesDetalle;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

//use Illuminate\Support\Facades\Validator;
use session;
use Dotenv\Regex\Result;
use App\Donables;
use App\Categoria;

class DonablesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $donablees = Donables::All();
        return  view('donables')->with('lista', $donablees);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $categorias = Categoria::All()->where('tipo', 'insumo_tipo');
        return  view('donables_add')->with('categorias', $categorias);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DonablesCreateRequest $request)
    {
        //recuperando parametros del formulario en la petición
        $nombre = $request->input("nombre");
        $descripcion = $request->input("descripcion");
        $idCategoria = $request->input("categoria");

        //construyendo el objeto
        $donable = new Donables();
        $donable->nombre = $nombre;
        $donable->descripcion = $descripcion;
        $donable->idCategoria = $idCategoria;

        //guardando el objeto
        try {
            if ($donable->save()) {
                # code...
                return  Redirect::to('donables'); //redirige a vista donde se muestra la lista de los registros
            } else {
                $request->session()->flash('messageErr', "**");
                return  Redirect::to('donables_add'); //->withErrors($validator)->withInput($request->all());//si hay error redirecciona al formulario
            }
        } catch (\Exception $e) {
            $request->session()->flash('messageErr', "**" . $e->getMessage());
            return  Redirect::to('donables_add'); //->withErrors($validator)->withInput($request->all()); //si hay error redirecciona al formulario
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    { }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $donable = DB::table('donables')
            ->where('idDonable', $id)
            ->select('donables.*')
            ->get();

        $categorias = Categoria::All()->where('tipo', 'insumo_tipo');

        if ($donable != null) {
            return  view('donables_mod')->with('donable', $donable[0])->with('categorias', $categorias); //retorna el registro encontrado a las vista del formularoi para modificar
        } else {
            Session::flash('messageErr', "La Categoria no existe o fue eliminada");
            return  Redirect::to('donables'); //Si no encuentra el objeto regresa a la vista donde se listan todos los objetos
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DonablesUpdateRequest $request)
    {
        $idDonable = $request->input("donable");
        $donable = Donables::find($idDonable);

        $nombre = $request->input("nombre");
        $descripcion = $request->input("descripcion");
        $idCategoria = $request->input("categoria");

        $donable->nombre = $nombre;
        $donable->descripcion = $descripcion;
        $donable->idCategoria = $idCategoria;

        try {
            if ($donable->save()) {
                # code...
                return  Redirect::to('donables'); //regresa al listado 
            } else {
                $request->session()->flash('messageErr', "**");
                return  Redirect::to('donables/donable/' . $idDonable); //->withErrors($validator)->withInput($request->all());//si hay error redirecciona al formulario 
            }
        } catch (Exception $e) {
            $request->session()->flash('messageErr', "**");
            return  Redirect::to('donables/donable/' . $idDonable); //->withErrors($validator)->withInput($request->all()); //si hay error redirecciona al formulario 
        }

        return Redirect::to("indexEntidad"); //regresa al listado 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $donable = Donables::find($id);

        if ($donable == null) {
            $request->session()->flash('messageErr', "Error while deleting area");
            return Redirect::to("donables");
        }
        try {
            if ($donable->delete()) {

                return  Redirect::to('donables');
            } else {
                $request->session()->flash('messageErr', "Error eliminando el Categoria");
                return Redirect::to("donables");
            }
        } catch (\Exception $e) {
            $request->session()->flash('messageErr', "**" . $e->getMessage());
            return Redirect::to("donables");
        }
    }
}
