<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Session;


class CategoriaController extends Controller
{
	//redireccion formulario para agregar categorias
     public function index(Request $request)
    {
        $categoria = DB::table('categoria')
            ->select('categoria.*')
            ->get();

        return  view('categorias')->with( array( 'lista' => $categoria));
    }

    //redireccion formulario para agregar categorias
     public function addForm(Request $request)
    {
        return view("categoria_add");
    }

    //agregando un nuevo registro
     public function create(Request $request)
    {
      	
		$nombre  =  $request->input("nombre");
		$descripcion = $request->input("desc"); 
		$tipo = $request->input("tipo");

		$rules = [
            'nombre' => 'required|unique:categoria',
            'desc'  => 'required',
            'tipo'  => 'required',            
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return  Redirect::to('createCategoria')->withErrors($validator)->withInput($request->all()); //si hay error redirecciona al formulario             
        }

        $myObject = new Categoria();
        $myObject->nombre = $nombre;
        $myObject->descripcion = $descripcion;
        $myObject->tipo = $tipo;

        try {
        	if ($myObject->save()) {
        		# code...
        		return  Redirect::to('indexCategoria'); //redirige a vista donde se muestra la lista de los registros

        	}else{
        		Session::flash('messageErr', "**". $e->getMessage());
            	return  Redirect::to('createCategoria')->withErrors($validator)->withInput($request->all());//si hay error redirecciona al formulario
        	}
        	
        } catch (Exception $e) {
        	Session::flash('messageErr', "**". $e->getMessage());
            return  Redirect::to('createCategoria')->withErrors($validator)->withInput($request->all()); //si hay error redirecciona al formulario
        }
    }
    //Busqueda del registro que se modificara
    public function find(Request $request, $id){
        
        $myObject = DB::table('categoria')
            ->where('idCat',$id)
            ->select('categoria.*')
            ->get();

        if ($myObject != null) {
            return  view('categoria_mod')->with(array('categoria'=> $myObject[0])); //retorna el registro encontrado a las vista del formularoi para modificar
        }else{
            Session::flash('messageErr', "La Categoria no existe o fue eliminada");
            return  Redirect::to('indexCategoria'); //Si no encuentra el objeto regresa a la vista donde se listan todos los objetos
        }
    }
    //Modificando el registro
    public function update(Request $request )
    {
        $idCat = $request->input("id");

        $myObject = Categoria::find($request->input("id"));

        if ($myObject == null) {
            return  Redirect::to("indexCategoria"); //si no se encuentra regresa a la lista
        }

        $nombre  =  $request->input("nombre");
		$descripcion = $request->input("desc"); 
		$tipo = $request->input("tipo");

		$rules = [
            'desc'  => 'required',
            'tipo'  => 'required',            
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return  Redirect::to('createCategoria')->withErrors($validator)->withInput($request->all()); //si hay error redirecciona al formulario             
        }
        
        $myObject->nombre = $nombre;
        $myObject->descripcion = $descripcion;
        $myObject->tipo = $tipo;

        try {
            if ($myObject->save()) {
                # code...
                return  Redirect::to('indexCategoria');//regresa al listado 
            }else{
                Session::flash('messageErr', "**". $e->getMessage());
                return  Redirect::to('findCategoria_'.$id )->withErrors($validator)->withInput($request->all());//si hay error redirecciona al formulario 
            }
            
        } catch (Exception $e) {
            Session::flash('messageErr', "**". $e->getMessage());
            return  Redirect::to('findCategoria_'.$id)->withErrors($validator)->withInput($request->all()); //si hay error redirecciona al formulario 
        }

        return Redirect::to("indexCategoria");//regresa al listado 

    }

    public function delete(Request $request, $id)
    {
        $myObject= Categoria::find($id);

        if ($myObject == null) {
            Session::flash('messageErr', "Error while deleting area");
            return Redirect::to("indexCategoria"); 
        }
        try{
            if($myObject->delete()){
                Session::flash('messageOk', "Categoria eliminada exitosamente");
                return  Redirect::to('indexCategoria');
            }else{
                Session::flash('messageErr', "Error eliminando el Categoria");
                return Redirect::to("indexCategoria"); 
            }
        } catch (Exception $e) {
            Session::flash('messageErr', "**". $e->getMessage());
            return Redirect::to("indexCategoria"); 
        }
    }
   

}
