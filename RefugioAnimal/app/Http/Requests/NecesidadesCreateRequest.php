<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NecesidadesCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo'=>'bail|required|max:100',
            'descripcion'=>'bail|required|max:255',
            'cantidad'=>'bail|required|numeric|min:0.00',
            'donable'=>'bail|required|integer|min:1|exists:donables,idDonable',
            'beneficiado'=>'bail|required|integer|min:1',
            'fecha_limite'=>'bail|after:tomorrow',
        ];
    }

    public function messages()
    {
        return [
            'titulo.required'=>'El titulo de la necesidad es requerido.',
            'titulo.max'=>'El titulo de la necesidad debe tener 100 caracteres como maximo.',
            'descripcion.required'=>'La descripcion de la necesidad es requerida.',
            'descripcion.max'=>'La descripcion de la necesidad debe tener 255 caracteres como maximo.',
            'cantidad.required'=>'La cantidad es requerida.',
            'cantidad.numeric'=>'La cantidad debe ser numerica',
            'cantidad.min'=>'La cantidad debe ser mayon a $0.00',
            'donable.required'=>'El donable es requerido.',
            'donable.integer'=>'El donable no es valido, formato invalido.',
            'donable.min'=>'El donable no es valido.',
            'donable.exists'=>'El donable seleccionado no existe.',
            'beneficiado.required'=>'El beneficiario es requerido.',
            'beneficiado.integer'=>'El beneficiario no es valido, formato invalido.',
            'beneficiado.min'=>'El beneficiario no es valido.',
            'fecha_limite.after'=>'La necesidad debe publicarse un dia como minimo',
        ];
    }
}

