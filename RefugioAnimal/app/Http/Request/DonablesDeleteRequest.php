<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DonablesDeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'=>'bail|required|integer|min:1|exists:donables,idDonable',
        ];
    }

    public function messages()
    {
        return [
            'id.required'=>'No has especificado el donable.',
            'id.integer'=>'El donable no es valido, formato invalido.',
            'id.min'=>'El donable no es valido.',
            'id.exists'=>'La categoria seleccionada no existe.',
        ];  
    }
}
