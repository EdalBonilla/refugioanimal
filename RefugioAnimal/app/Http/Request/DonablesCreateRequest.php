<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DonablesCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'=>'bail|required|max:100',
            'descripcion'=>'bail|required|max:255',
            'categoria'=>'bail|required|integer|min:1|exists:categoria,idCat',
        ];
    }

    public function messages()
    {
        return [
            'nombre.required'=>'El nombre de el donable es requerido.',
            'nombre.max'=>'El nombre debe tener 100 caracteres como maximo.',
            'descripcion.required'=>'La descripcion de el donable es requerido.',
            'descripcion.max'=>'La descripcion de el donable debe tener 255 caracteres como maximo.',
            'categoria.required'=>'La categoria de el donable es requerida.',
            'categoria.integer'=>'La categoria no es valida formato invalido.',
            'categoria.min'=>'La categoria no es valida.',
            'categoria.exists'=>'La categoria seleccionada no existe.',
        ];  
    }
}

