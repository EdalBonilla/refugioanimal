<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonacionesDetalle extends Model
{
    //
    protected $table = 'detallesdonacion';

    protected $primaryKey= "idDetalle";

public function tipo(){
    return $this->belongsTo(Categoria::class,'idCategoria');
}

    public function donacion(){
        return $this->belongsTo(Donaciones::class,'idDonaciones');
    }

    public function necesidad(){
        return $this->belongsTo(Necesidades::class,'idNecesidad');
    }
}
