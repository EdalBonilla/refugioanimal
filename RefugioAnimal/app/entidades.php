<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entidades extends Model
{
    //
    protected $table = 'entidades';

    protected $primaryKey= "idEntidad";

    public function donaciones(){
        return $this->hasMany(Donaciones::class);
    }
    
    public function permission(){
        return $this->belongsTo(Categoria::class,'categoria');
    }
}
