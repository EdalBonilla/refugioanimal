<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donables extends Model
{
    //
    protected $table = 'donables';

    protected $primaryKey= "idDonable";

    public function necesidades(){
        return $this->hasMany(Necesidades::class,'idDonable');
    }

    public function categoria(){
        return $this->belongsTo(Categoria::class,'idCategoria');
    }
}
