<?php
include 'menu.php';
?>
<?php 

	$idMascota= $mascota->idMascota;
	$nombre= old('nombre') ? old('nombre') : $mascota->nombre;
	$foto= old('foto') ? old('foto') : $mascota->foto;
	$sexo= old('sexo') ? old('sexo') : $mascota->sexo;
	$edad= old('edad') ? old('edad') : $mascota->edad;
	$fecha= old('fecha') ? old('fecha') : $mascota->fechaIngreso;
	$estado= old('estado') ? old('estado') : $mascota->estado;		
	$raza= old('raza') ? old('raza') : $mascota->raza;
	$condicion= old('condicion') ? old('condicion') : $mascota->condicion;
	$est= old('est') ? old('est') : $mascota->esterelizado;
	$especie= old('especie') ? old('especie') : $mascota->especie;		



	?>
<div class="jumbotron text-center" id="cajaUsario">
	<div class="text-center">
		<h1>Editar Cachorro </h1>  
	</div>
	
</div>
<div class="row" style="    flex-wrap: wrap;
margin-right: 0;
margin-left: 0">
<div class="col-md-4 text-center">

	<img src="images/{{$foto}}" id="avatar" style=" width: 100%;">

</div>
<div class="col-md-7 " id="cajadd2">
	<h5 class="text-center" id="tituloadd">Nombre: {{ $nombre }}</h5>
	<hr>

	
	<form action="updateMascota" method="POST" class="form-horizontal " enctype="multipart/form-data">

		<input type="hidden" name="_token" value="{{ csrf_token() }}"/>
		
		<input type="hidden" name="id" value="{{ $idMascota }}">

		
		<div class="form-row">
			<div class="col-md-6 mb-3">
				<label  for="name" >Nombre Mascota</label>
			
					<input type="text" class="form-control" id="name" name="nombre" placeholder="Ingrese el nombre del área" value="{{ $nombre }}" required>
					@if($errors->has('nombre'))
					<span class="label label-warning">{{$errors->first('nombre')}}</span>
					@endif 	
			
			</div> 
			
		
			<div class="col-md-6 mb-3">
				<label for="Edad" >Edad</label>
				
					<input type="text" class="form-control" id="tipo" name="edad" placeholder="ingrese el carnet del encargado" value="{{ $edad }}">
					@if($errors->has('edad'))
					<span class="label label-warning">{{$errors->first('edad')}}</span>
					@endif 
				
			</div> 
		</div>

		<div class="form-row">
		<div class="col-md-6 mb-3">
			<label for="name" >sexo</label>
			
				<select id="inputState" name="sexo" class="form-control">
					<?php if ($sexo == 1): ?>
			       <option value="{{ $sexo }}">Macho</option>
			       <?php else: ?>
			       <option value="{{ $sexo }}">Hembra</option>
			       <?php endif ?>
					<option value="1">Macho</option>
					<option value="2">Hembra</option>	    				
				</select>
				@if($errors->has('sexo'))
				<span class="label label-warning">{{$errors->first('sexo')}}</span>
				@endif 	
			
		</div> 
		<div class="col-md-6 mb-3">
			<label for="fechaIngreso" >fechaIngreso</label>

				<input type="text" class="form-control" id="descripcion" name="fecha" placeholder="Ingrese la descripción del área" value="{{ $fecha }}">
				@if($errors->has('fecha'))
				<span class="label label-warning">{{$errors->first('fecha')}}</span>
				@endif 
			
		</div>
		</div> 

		<div class="form-row">
		<div class="col-md-6 mb-3">
			<label class="col-md-4" for="raza" >raza</label>
			<select name="raza" id="inputState" class="form-control">
					@foreach($razas as $raza)
					<option value="{{$raza->idRaza}}">{{$raza->nombre}}</option>
				@endforeach
			</select>			
		</div> 

		<div class="col-md-6 mb-3">
			<label class="col-md-4" for="condicion" >condicion</label>
			
				<input type="text" class="form-control" id="descripcion" name="condicion" placeholder="Ingrese la descripción del área" value="{{ $condicion }}">
				@if($errors->has('condicion'))
				<span class="label label-warning">{{$errors->first('condicion')}}</span>
				@endif 
			
		</div> 
		</div>
		
		<div class="form-row">
		<div class="col-md-6 mb-3">
			<label class="col-md-4" for="est" >esterelizado</label>
			
				<select name="est" id="inputState" class="form-control">				 		
					<option value="0">SI</option>
					<option value="1">NO</option>    						    					
				</select> 
				@if($errors->has('est'))
				<span class="label label-warning">{{$errors->first('est')}}</span>
				@endif 
			
		</div> 
		<div class="col-md-6 mb-3">
			<label class="col-md-4" for="especie" >especie</label>
			
				<input type="text" class="form-control" id="name" name="especie" placeholder="Ingrese el nombre del área" value="{{ $especie }}">
				@if($errors->has('especie'))
				<span class="label label-warning">{{$errors->first('especie')}}</span>
				@endif 	
			
		</div>
		</div> 

		<div class="col-md-12">
			<center>
				<br>
				<input type="submit" class="btn btn-sm btn-success" name="submit" value="GUARDAR">
				<a href="{{ URL::previous() }}" title="Regresar"  class="btn btn-sm btn-secondary"><i class="fa fa-trash" aria-hidden="true"></i> CANCELAR</a>
			</center>
		</div>
		
	</form>
	<script>
      // Example starter JavaScript for disabling form submissions if there are invalid fields
      (function() {
      	'use strict';
      	window.addEventListener('load', function() {
          // Fetch all the forms we want to apply custom Bootstrap validation styles to
          var forms = document.getElementsByClassName('needs-validation');
          // Loop over them and prevent submission
          var validation = Array.prototype.filter.call(forms, function(form) {
          	form.addEventListener('submit', function(event) {
          		if (form.checkValidity() === false) {
          			event.preventDefault();
          			event.stopPropagation();
          		}
          		form.classList.add('was-validated');
          	}, false);
          });
      }, false);
      })();
  </script>
</div>
</div>



<?php
include "footer.php";
?>
