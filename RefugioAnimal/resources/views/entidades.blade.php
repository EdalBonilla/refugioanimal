<?php
include 'menu.php';
?>

<div class="jumbotron text-center" id="cajaUsario">
	<div class="text-center">
		<h1>Listado de Usuarios </h1>  
	</div>
	
</div>
<a href="createEntidad" title="new" class="btn btn-success"><i class="fa fa-pencil" aria-hidden="true"></i> Registrar Nuevo</a>
	<table class="table" id="categorias_table">
		<thead class="thead-dark">
			<tr>
				<th>#</th><th>nombre</th><th>email</th><th>cel</th><th>categoria</th><th>estado</th><th>Opciones</th>
			</tr>
		</thead>
		<tbody>
			<?php $counter=1;  ?>
			@foreach ($lista as $item)
			<tr>
					<td>{{ $counter  }}</td>
					<td>{{ $item->nombre }}</td>
					<td>{{ $item->email }}</td>
					<td>{{ $item->telefono }}</td>
					<td>@if ($item->categoria == 10)
       Administrador
     @endif</td>
					
					<td>Activo</td>
					<td>
				<a href="adminEntidad_{{$item->idUsers}}" title="Administrar" class="btn btn-sm btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i>VER</a>
		   		<a href="findEntidad_{{$item->idEntidad}}" title="Editar" class="btn btn-sm btn-info"><i class="fa fa-pencil" aria-hidden="true"></i> EDITAR</a>
		    	<a href="deleteEntidad_{{$item->idEntidad }}" title="Eliminar"  class="btn btn-sm btn-danger delete">
		    	<i class="fa fa-trash" aria-hidden="true">
		    	</i> ELIMINAR
		    	</a>
		   			 </td>
					
			</tr>
				<?php $counter++; ?>
			@endforeach
		</tbody>
	</table>
		
<?php
include "footer.php";
?>