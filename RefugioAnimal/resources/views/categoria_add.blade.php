<?php
include 'menu.php';
?>
<div class="jumbotron text-center" id="cajaUsario">
	<div class="text-center">
		<h1>Agregar categorias </h1>  
	</div>
	
</div>
<form action="categoria_add" method="POST" class="form-horizontal " enctype="multipart/form-data">

		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		
		<div class="form-group">
			<label class="col-md-4" for="name" >Nombre Categoria</label>
				<div class="col-md-8">
					<input type="text" class="form-control" id="name" name="nombre" placeholder="Ingrese el nombre del área" value="{{ old('nombre') }}">
					@if($errors->has('nombre'))
						<span class="label label-warning">{{$errors->first('nombre')}}</span>
					@endif 	
				</div>
		</div> 
		<div class="form-group">
			<label class="col-md-4" for="desc" >Descripción</label>
				<div class="col-md-8">
					<input type="text" class="form-control" id="descripcion" name="desc" placeholder="Ingrese la descripción del área" value="{{ old('desc') }}">
					@if($errors->has('desc'))
						<span class="label label-warning">{{$errors->first('desc')}}</span>
					@endif 
				</div>
		</div> 
		<div class="form-group">
			<label class="col-md-4" for="enc" >tipo</label>
				<div class="col-md-8">
					<input type="text" class="form-control" id="tipo" name="tipo" placeholder="ingrese el carnet del encargado" value="{{ old('tipo') }}">
					@if($errors->has('tipo'))
						<span class="label label-warning">{{$errors->first('tipo')}}</span>
					@endif 
				</div>
		</div> 


        <div class="col-md-12">
        	<center>
        		<br>
				<input type="submit" class="btn btn-sm btn-primary" name="submit" value="GUARDAR">
					<a href="{{ URL::previous() }}" title="Regresar"  class="btn btn-sm btn-secondary"><i class="fa fa-trash" aria-hidden="true"></i> CANCELAR</a>
			</center>
		</div>
		
	</form>
<?php
include "footer.php";
?>