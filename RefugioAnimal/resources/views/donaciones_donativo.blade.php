<?php
include 'menu.php';
?>
@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/donantes.css') }}">
@endpush

<div class="page-header">
	<h1>Donaciones</h1>
</div>

<div class="container">
	<div class="row">
		<div class="col-sm-8">

			<div class="panel">
				<div class="panel-content">
					<div>
						<h4 class="side-title">{{$necesidad->titulo}}</h4>
					</div>
					<div class="detailed-description">
						<p>{{$necesidad->descripcion}}</p>
					</div>
					
					<div class="detailed-summary">
						Meta ${{$necesidad->cantidad}} Recaudado ${{$totalDonaciones}}
						<div class="progress">
							<div class="progress-bar progress-bar-striped bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
						<div class="-description">
						Fecha Limite: {{$necesidad->fechaFin}}
					</div>
					</div>


					<div>
						<h3 class="side-title">Donaciones Realizadas</h3>
					</div>
					
					<!-- Donadores -->
					<div class="single category">
						
						<div class="">
							<ul class="list-unstyled scrollbox">

								@foreach($donacionesRealizadas as $donacionDetalle)
								<li><a href="" title="">{{$donacionDetalle->donacion->entidad->nombre}} <span class="pull-right">${{$donacionDetalle->cantidad}}</span></a></li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>
			</div>

		</div>

		<div class="col-sm-4">
			<!-- Donadores 
			<div class="single category">
				<h3 class="side-title">Donaciones Realizadas</h3>
				<div class="">
					<ul class="list-unstyled scrollbox">

						@foreach($donacionesRealizadas as $donacionDetalle)
						<li><a href="" title="">{{$donacionDetalle->donacion->entidad->nombre}} <span class="pull-right">${{$donacionDetalle->cantidad}}</span></a></li>
						@endforeach
					</ul>
				</div>
				<div class="foot-total foot-text">
					Total Recaudado<span class="pull-right">${{$totalDonaciones}}</span>
				</div>

			</div>-->
			<form action="/donaciones/donativo" method="POST">
				<input type="hidden" name="idNecesidad" value="{{$necesidad->idNecesidad}}">
				{{ csrf_field() }}
				<div>
					<h4>Monto a donar</h4>
					<input name="monto" type="number" step="0.01" />
				</div>
				<select name="periodoDePago" id="formaDePago">
					<option value="Pago unico" >Pago unico</option>
					<option value="Mensual" >Mensual</option>
					<option value="Anual" >Anual</option>
				</select>
				<select name="formaDePago" id="formaDePago">
					@foreach($formasDePago as $forma)
						<option value="{{$forma->idFormaPago}}" >{{$forma->nombre}}</option>
					@endforeach
				</select>

				<input type="submit" class="btn btn-warning" value="Hacer Donativo">
				<a href="/necesidades" class="btn btn-success">Cancelar</a>
			</div>
		</div>
	</form>
</div>
<?php
include "footer.php";
?>
