<?php 

include 'menu.php';


		$idCat= $categoria->idCat;
		$nombre= old('nombre') ? old('nombre') : $categoria->nombre;
		$descripcion= old('desc') ? old('desc') : $categoria->descripcion;
		$tipo= old('tipo') ? old('tipo') : $categoria->tipo;
		
		
	?>
	<div class="jumbotron text-center" id="cajaUsario">
	<div class="text-center">
		<h1>Editar categorias </h1>  
	</div>
	
</div>
<form action="updateCategoria" method="POST" class="form-horizontal " enctype="multipart/form-data">

		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<input type="hidden" name="id" value="{{ $idCat }}">

		
		<div class="form-group">
			<label class="col-md-4" for="name" >Nombre Categoria</label>
				<div class="col-md-8">
					<input type="text" class="form-control" id="name" name="nombre" placeholder="Ingrese el nombre del área" value="{{ $nombre }}">
					@if($errors->has('nombre'))
						<span class="label label-warning">{{$errors->first('nombre')}}</span>
					@endif 	
				</div>
		</div> 
		<div class="form-group">
			<label class="col-md-4" for="desc" >Descripción</label>
				<div class="col-md-8">
					<input type="text" class="form-control" id="descripcion" name="desc" placeholder="Ingrese la descripción del área" value="{{ $descripcion }}">
					@if($errors->has('desc'))
						<span class="label label-warning">{{$errors->first('desc')}}</span>
					@endif 
				</div>
		</div> 
		<div class="form-group">
			<label class="col-md-4" for="enc" >tipo</label>
				<div class="col-md-8">
					<input type="text" class="form-control" id="tipo" name="tipo" placeholder="ingrese el carnet del encargado" value="{{ $tipo }}">
					@if($errors->has('tipo'))
						<span class="label label-warning">{{$errors->first('tipo')}}</span>
					@endif 
				</div>
		</div> 


        <div class="col-md-12">
        	<center>
        		<br>
				<input type="submit" class="btn btn-sm btn-primary" name="submit" value="GUARDAR">
					<a href="{{ URL::previous() }}" title="Regresar"  class="btn btn-sm btn-secondary"><i class="fa fa-trash" aria-hidden="true"></i> CANCELAR</a>
			</center>
		</div>
		
	</form>
<?php
include 'footer.php';
?>