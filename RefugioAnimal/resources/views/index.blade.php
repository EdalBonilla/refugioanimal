<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="../css/inicio.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Knewave&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Signika&display=swap" rel="stylesheet">
  <title>Salvadog</title>

</head>

<body style="background-color: #ffff; ">
  <script type="text/javascript">
  // Select all links with hashes
  $('a[href*="#"]')
// Remove links that don't actually link to anything
.not('[href="#"]')
.not('[href="#0"]')
.click(function(event) {
  // On-page links
  if (
    location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
    &&
    location.hostname == this.hostname
    ) {
    // Figure out element to scroll to
  var target = $(this.hash);
  target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    // Does a scroll target exist?
    if (target.length) {
      // Only prevent default if animation is actually gonna happen
      event.preventDefault();
      $('html, body').animate({
        scrollTop: target.offset().top
      }, 1000, function() {
        // Callback after animation
        // Must change focus!
        var $target = $(target);
        $target.focus();
        if ($target.is(":focus")) { // Checking if the target was focused
          return false;
        } else {
          $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
          $target.focus(); // Set focus again
        };
      });
    }
  }
});


</script>

<nav id="menu" class="navbar fixed-top navbar-expand-lg na   navbar-dark  " >
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="nav navbar-nav ">
      <li class="nav-item active">
        <a class="nav-link" href="#fondo1">Salvadog <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href  ="#fondo3">Acerca de</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#fondo4">Servicios</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#fondo5">Nuestra familia</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#fondo-footer">Contacto</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="mascota_lista">Apadrina un cachorro</a>
      </li>
@if (Auth::check())
    <li class="nav-item">
        <a class="nav-link" href="adminEntidad">Administrar</a>
      </li>
    <a href="logout">
    <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Cerrar Sesión</button></a>
@else
    <a href="login">
    <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Iniciar Sesión</button></a>
@endif
    </ul>

  </div>
  
  </nav>

  <div class="fondo" id="fondo1" >

    <div class="row" style="margin-right: 0; margin-left: 0;">
      <div class="col-lg-6 col-sm-7">
        <h2 style="" id="htitulo">Salvadog - Apadrina o Adopta un nuevo amigo</h2>
      </div>

    </div>



  </div>
  <div class="fondo" id="fondo3">
      <div class="row" style="margin-right: 0; margin-left: 0; text-align: center;">

        <div class="col-md-7 mb-5">
          <div id="caja-texto2">
            <p id="tex3">¿Quiénes somos?</p>
          </div>
          <div id="caja-texto3">
            <p>Somos un refugio que ayuda a animales quienes no han tenido la suerte de haber nacido dentro de una familia que los ame, ayúdamos a los más desamparados, aquellos por los que nadie velaría, y gracias a eso hemos podido encontrarla hogar a muchos animales, ayúdanos a ayudarlos.</p>
          </div>
        </div>
    </div>
  </div>
  <div class="fondo" id="fondo4" >
    <div class="container">
      <div class="row" style="margin-right: 0; margin-left: 0; text-align: center;">
        <div class="caja-title col-md-12 mb-3" style="margin-bottom: 3rem!important;">
          <h2 class="title2">Lo que hacemos</h2>
          <hr>
          <div class="container" style="width: 78%;">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
            labore et dolore magna aliqua, Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
            labore et dolore magna aliqua</p>
          </div>
        </div>
        <div class="col-md-3 mb-3" style="background-color:#C0392B;color :white;padding: 5%;">
          <img src="../img/dog.png" class="img-responsive" alt="">
          <div class="row caja-texto4"  style="margin-left: 0px;" >
            <h4 class="title1"> Adopta</h4>
            <p>Ayúdanos a encontrar un nuevo hogar a un peludo que no tuvo la suerte de haber nacido dentro de una familia.</p>
          </div>
        </div>
        <div class="col-md-3 mb-3" style="background-color: #9B59B6;color :white;padding: 5%;">
          <img src="../img/bone.png" class="img-responsive" alt="">
          <div class="row caja-texto4"   style="margin-left: 0px;" >
            <h4 class="title1">Apadrina</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
            labore et dolore magna aliqua.</p>
          </div>
        </div>
        <div class="col-md-3 mb-3" style="background-color: #2980B9;color :white;padding: 5%;" >
          <img src="../img/steeps.png" class="img-responsive" alt="">

          <div class="row caja-texto4"  style="margin-left: 0px;" >
            <h4 class="title1">Apadrina</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
            labore et dolore magna aliqua.</p>
          </div>
        </div>
        <div class="col-md-3 mb-3" style="background-color:#1ABC9C;color :white;padding: 5%;">
          <img src="../img/dog.png" class="img-responsive" alt="">
          <div class="row caja-texto4"  style="margin-left: 0px;"  >
            <h4 class="title1">Apadrina</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
            labore et dolore magna aliqua.</p>
          </div>
        </div>
        <div class="col-md-6 mb-3" >
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incidid
            unt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
          ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
        </div>
        <div class="col-md-6 mb-3" >
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incidid
            unt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
          ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
        </div>

      </div>
    </div>
  </div>
  <div  id="fondo5">
    <h2>Nuestra Familia</h2>

    <div class="row" style="margin-left:10%; margin-right: 10%; text-align: center;">
      <div class="col-md-6 mb-3" id="family_1" >
        <div class="family_2">
        </div>
      </div>

      <div class="col-md-6 mb-3">
        <div class="container">
          <div class=" family_3"  >
            <h3>Diana paola</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incidid
              unt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
            ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
          </div>
        </div>
      </div>

      <div class="col-md-6 mb-3" id="family_5" >
        <div class="family_2">
        </div>
      </div>

      <div class="col-md-6 mb-3">
        <div class="container">
          <div class=" family_3"  >
            <h3>Jhon Paulo</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incidid
              unt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
            ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
          </div>
        </div>
      </div>


    </div>
    <div id="fondo-footer" >
      <h2 class="title2" >Ponte en contacto</h2>
      <hr/>
      <!-- Footer -->
      <footer class="page-footer font-small teal pt-4">

        <!-- Footer Text -->
        <div class="container-fluid text-center text-md-left">

          <!-- Grid row -->
          <div class="row texfooter" >

            <!-- Grid column -->
            <div class="col-md-6 mt-md-0 mt-3">

              <!-- Content -->
              <h5 class="text-uppercase font-weight-bold title5">Dirección</h5>

              <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Optio deserunt fuga perferendis modi earum
                commodi aperiam temporibus quod nulla nesciunt aliquid debitis ullam omnis quos ipsam, aspernatur id
              excepturi hic.</p>

            </div>
            <!-- Grid column -->

            <hr class="clearfix w-100 d-md-none pb-3">

            <!-- Grid column -->
            <div class="col-md-6 mb-md-0 mb-3">

              <!-- Content -->

              <h5 class="text-uppercase font-weight-bold title5">Correo electrónico</h5>
              <p class="texfooder">Lorem ipsum, dolor sit amet consectetur adipisicing elit.</p>
              <h5 class="text-uppercase font-weight-bold title5">Teléfonos</h5>
              <p class="texfooder">+503 2222-2222 / +503 7777-7777.</p>



            </div>
            <!-- Grid column -->

          </div>
          <!-- Grid row -->

        </div>
        <!-- Footer Text -->
        <hr/>
        <!-- Copyright -->
        <div class="footer-copyright text-center py-3">© 2019 Copyright:
          <a href="#"> Salvadog.com</a>
        </div>
        <!-- Copyright -->

      </footer>
      <!-- Footer -->
    </div>
  </div>


  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script type="text/javascript">
    $(window).scroll(function() {
     if ($("#menu").offset().top > 56) {00
       $("#menu").addClass("bg-dark");
     } else {
       $("#menu").removeClass("bg-dark");
     }
   });
 </script>
</body>

</html>
