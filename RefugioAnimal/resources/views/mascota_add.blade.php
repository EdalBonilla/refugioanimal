<?php
include 'menu.php';
?>


<div class="row" style="    flex-wrap: wrap;
margin-right: 0;
margin-left: 0">
<div class="col-md-4 text-center">

	<img src="images/addpet.png" id="avatar">

</div>
<div class="col-md-7 " id="cajadd2">
	<h5 class="text-center" id="tituloadd">Ingreso de nueva mascota</h5>
	<hr>
	<form action="mascota_add" method="POST" class="form-validation" enctype="multipart/form-data">

		{{ csrf_field() }}

		<div class="form-row">
			<div class="col-md-6 mb-3">
				<label  for="name" >Nombre Mascota</label>

				<input type="text" class="form-control" id="name" name="nombre" placeholder="Ingrese el nombre de la mascota" value="{{ old('nombre') }}">

				<div class="valid-feedback">
					Dato correcto!
				</div>
				<div class="invalid-feedback">
					@if($errors->has('nombre'))
					<span class="label label-warning">{{$errors->first('nombre')}}</span>
					@endif				         						

				</div>
			</div> 

			<div class="col-md-6 mb-3">
				<label  for="Edad" >Edad</label>

				<input type="text" class="form-control" id="tipo" name="edad" placeholder="ingrese la Edad" value="{{ old('edad') }}">
				<div class="valid-feedback">
					Dato correcto!
				</div>
				<div class="invalid-feedback">
					@if($errors->has('edad'))
					<span class="label label-warning">{{$errors->first('edad')}}</span>
					@endif
				</div> 						
			</div>
		</div>

		<div class="form-row">
			<div class="col-md-6 mb-3">
				<label class="col-md-4" for="name" >sexo</label>
				<select id="inputState" name="sexo" class="form-control">
					<option value="1">Macho</option>
					<option value="2">Hembra</option>	    				
				</select>    					
				<div class="valid-feedback">
					Dato correcto!
				</div>
				<div class="invalid-feedback">
					@if($errors->has('edad'))
					<span class="label label-warning">{{$errors->first('edad')}}</span>
					@endif
				</div> 	
			</div>

			<div class="col-md-6 mb-3">
				<label class="col-md-4" for="fechaIngreso" >fechaIngreso</label>

				<input type="date" class="form-control" id="descripcion" name="fecha" placeholder="Ingrese la descripción del área" value="{{ old('fecha') }}">
				<div class="valid-feedback">
					Dato correcto!
				</div>
				@if($errors->has('fecha'))	
					<br>				
					<span class="alert alert-warning" role="alert">{{$errors->first('fecha')}}</span>					
				@endif				         						

			</div>
		</div>

		<div class="form-row">
			<div class="col-md-6 mb-3">
				<label class="col-md-4" for="estado" >estado</label>
				<select id="inputState" name="estado" class="form-control">
					@foreach($estados as $estado)
					<option value="{{$estado->idCat}}">{{$estado->nombre}}</option>
					@endforeach

				</select>
			</div>

			<div class="col-md-6 mb-3">
				<label for="estado" >Foto</label>					
				<input type="file" accept="image/*" class="form-control" id="file-input" name="imgFile" required />					
			</div> 
		</div>

		<div class="form-row">
			<div class="col-md-6 mb-3">
				<label  for="raza" >raza</label>
				<select name="raza" id="inputState" class="form-control">
					@foreach($razas as $raza)
					<option value="{{$raza->idRaza}}">{{$raza->nombre}}</option>
					@endforeach
				</select>
			</div>

			<div class="col-md-6 mb-3">
				<label for="condicion" >condicion</label>

				<input type="text" class="form-control" id="descripcion" name="condicion" placeholder="Ingrese la condicion del animal" value="{{ old('condicion') }}" required>
				@if($errors->has('condicion'))
				<span class="label label-warning">{{$errors->first('condicion')}}</span>
				@endif 

			</div>
		</div>

		<div class="form-row">
			<div class="col-md-6 mb-3">
				<label  for="est" >esterelizado</label>					
				<select name="est" id="inputState" class="form-control"> 		
					<option value="0">NO</option>
					<option value="1">SI</option>    						    					
				</select> 					
			</div>

			<div class="col-md-6 mb-3">
				<label  for="especie" >tipo</label>					
				<select name="especie" class="form-control">
					@foreach($especies as $especie)
					<option value="{{$especie->idCat}}">{{$especie->nombre}}</option>
					@endforeach	    					
				</select>					
			</div>
		</div> 

		<div class="col-md-12">
			<center>
				<br>
				<input type="submit" class="btn  btn-success" name="submit" value="GUARDAR">
				<a href="mascotas" class="btn  btn-info">CANCELAR</a>
			</center>
		</div>
		
	</form>

	<script>
      // Example starter JavaScript for disabling form submissions if there are invalid fields
      (function() {
      	'use strict';
      	window.addEventListener('load', function() {
          // Fetch all the forms we want to apply custom Bootstrap validation styles to
          var forms = document.getElementsByClassName('needs-validation');
          // Loop over them and prevent submission
          var validation = Array.prototype.filter.call(forms, function(form) {
          	form.addEventListener('submit', function(event) {
          		if (form.checkValidity() === false) {
          			event.preventDefault();
          			event.stopPropagation();
          		}
          		form.classList.add('was-validated');
          	}, false);
          });
      }, false);
      })();
  </script>
</div>
</div>



<?php
include "footer.php";
?>
