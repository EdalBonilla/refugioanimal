

<?php
include 'menu.php';
 ?>
 <?php
  $idEntidad = $entidades->idEntidad;
  $nombre = old('nombre')?old('nombre') : $entidades->nombre;
  $email = old('email')?old('email') : $entidades->email;
  $telefono = old('telefono')?old('telefono') : $entidades->telefono;
  $categoria = old('categoria')?old('categoria') : $entidades->categoria;  
  $estado = old('estado')?old('estado') : $entidades->estado;

?>

<div class="jumbotron text-center" id="cajaUsario">
  <div class="text-center">
</div>
<h1> BIENVENIDO <br>{{ $nombre }}</h1>
<p></p>
</div>
<div class="container">
<div class="row">

 <div class="col-sm-4">
   <h3>Datos del usuario </h3>
   <div class="row">

   </div>
   <ul class="" style="ist-style:none;">
     <li><b>Nombre :</b> {{ $nombre }} </li>
     <li><b>Correo :</b> {{ $email }} </li>
     <li><b>Telefono :</b> {{ $telefono }} </li>
     <li><b>Direccion :</b> Desconocida  </li>
     <li><b>Tipo de usuario :</b> {{Auth::user()->entidad->permission->nombre}}  </li>
     <li><b>Tipo Usuario :</b>@if ($categoria == 10)
       Administrador
     @endif</li>

   </ul>
   <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
   <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
 </div>
 <div class="col-sm-4" id="cajaUsario2">
  <h3>Historial de donaciones</h3>
     <table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Codigo</th>
      <th scope="col">Mes</th>
      <th scope="col">Monto</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">0246</th>
      <td>Agosto</td>
      <td>$150</td>

    </tr>
    <tr>
      <th scope="row">0269</th>
      <td>Diciembre</td>
      <td>$100</td>

    </tr>
    <tr>
      <th scope="row">03644</th>
      <td>Enero</td>
      <td>$50</td>
    </tr>
    <tr>
      <th></th>
      <th> total</th>
      <th scope="row">$300</th>

    </tr>
  </tbody>
</table>
 
 </div>
 <div class="col-sm-4">
   <h3>Opciones</h3>
   <a href="mascotas" class="btn btn-success"> ver animales</a>
   <br><br>
   <a href="necesidades/necesidad/1" class="btn btn-primary">Donar</a> 
   <br><br>
   <a href="findEntidad_{{$idEntidad}}" class="btn btn-danger">Editar</a> 

</div>
</div>
</div>
<?php
include "footer.php";
 ?>
