<?php
include 'menu.php';
?>

<div class="jumbotron text-center" id="cajaUsario">
	<div class="text-center">
		<h1>Listado de Donables </h1>  
	</div>
	
</div>
<div>
	<table  id="myTable" class="table table-striped  dt-responsive nowrap">
		<thead class="thead-dark">
			<tr>
				<th>#</th>
				<th>Nombre</th>
				<th>Categoria</th>
				<th>Descripcion</th>
				<th>Opciones</th>
			</tr>
		</thead>
		<tbody>
			<?php $counter = 1;  ?>
			@foreach ($lista as $item)
			<tr>
				<td>{{ $counter  }}</td>
				<td>{{ $item->nombre }}</td>
				<td>
				    @if($item->categoria !=null)
				        {{ $item->categoria->nombre }}
				    @endif
				</td>
				<td>{{ $item->descripcion }}</td>

				<td>
					<a href="donables/donable/{{$item->idDonable}}" title="Editar" class="btn btn-sm btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a>
					<a href="donables/eliminar/{{$item->idDonable }}" title="Eliminar" class="btn btn-sm btn-danger delete">
						<i class="fa fa-trash" aria-hidden="true">
						</i> ELIMINAR
					</a>
				</td>
			</tr>
			<?php $counter++; ?>
			@endforeach
		</tbody>
	</table>

</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#entidades_table').DataTable();
	});
</script>

<?php
include "footer.php";
?>
