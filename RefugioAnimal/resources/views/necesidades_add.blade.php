<?php
include 'menu.php';
?>
<div class="jumbotron text-center" id="cajaUsario">
	<div class="text-center">
		<h1>Listado de Usuarios </h1>  
	</div>
	
</div>

	<form action="/necesidades/crear" method="POST">
		{{ csrf_field() }}
		<div class="panel">
			<div class="panel-content">
				<div>
					<h4>Titulo</h4>
					<input name="titulo" type="text">
					@if($errors->has('titulo'))
						<span class="label label-warning">{{$errors->first('titulo')}}</span>
					@endif 
				</div>
				<div>
					<h4>Descripción</h4>
					<input name="descripcion" type="text" />
					@if($errors->has('descripcion'))
						<span class="label label-warning">{{$errors->first('descripcion')}}</span>
					@endif 
				</div>
				<div>
					<h4>Valor necesitado</h4>
					<input type="number" name="cantidad" id="cantidad" step="0.01">
					@if($errors->has('cantidad'))
						<span class="label label-warning">{{$errors->first('cantidad')}}</span>
					@endif 
				</div>
				<div>
					<h4>Donable</h4>
					<select name="donable" id="donable">
						@foreach($donables as $donable)
							<option value="{{$donable->idDonable}}">{{$donable->nombre}}</option>
						@endforeach
					</select>
					@if($errors->has('donable'))
						<span class="label label-warning">{{$errors->first('donable')}}</span>
					@endif 
				</div>
				<div>
					<h4>Fecha limite</h4>
					<input type="date" name="fecha_limite" id="fecha_limite">
					@if($errors->has('fecha_limite'))
						<span class="label label-warning">{{$errors->first('fecha_limite')}}</span>
					@endif 
				</div>
				<input type="submit" value="Guardar">
			</div>
		</div>
	</form>

<?php
include "footer.php";
?>
