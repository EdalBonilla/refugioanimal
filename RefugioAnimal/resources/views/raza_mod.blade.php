<?php 

		$idRaza= $raza->idRaza;
		$nombre= old('nombre') ? old('nombre') : $raza->nombre;
		$descripcion= old('desc') ? old('desc') : $raza->descripcion;
		
		
		
	?>
<form action="updateRaza" method="POST" class="form-horizontal " enctype="multipart/form-data">

		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="_field" value="{{ csrf_field() }}">

		<input type="hidden" name="id" value="{{ $idRaza }}">

		
		<div class="form-group">
			<label class="col-md-4" for="name" >Raza</label>
				<div class="col-md-8">
					<input type="text" class="form-control" id="name" name="nombre" placeholder="Ingrese el nombre del área" value="{{ $nombre }}">
					@if($errors->has('nombre'))
						<span class="label label-warning">{{$errors->first('nombre')}}</span>
					@endif 	
				</div>
		</div> 
		<div class="form-group">
			<label class="col-md-4" for="desc" >Descripción</label>
				<div class="col-md-8">
					<input type="text" class="form-control" id="descripcion" name="desc" placeholder="Ingrese la descripción del área" value="{{ $descripcion }}">
					@if($errors->has('desc'))
						<span class="label label-warning">{{$errors->first('desc')}}</span>
					@endif 
				</div>
		</div> 		
        <div class="col-md-12">
        	<center>
        		<br>
				<input type="submit" class="btn btn-sm btn-primary" name="submit" value="GUARDAR">
				<input type="reset" class="btn btn-sm btn-default" value="CANCELAR">
			</center>
		</div>
		
	</form>