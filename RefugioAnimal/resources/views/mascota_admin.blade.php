<?php
include 'menu.php';

$idMascota= $mascota->idMascota;
$nombre= old('nombre') ? old('nombre') : $mascota->nombre;
$foto= old('foto') ? old('foto') : $mascota->foto;
$sexo= old('sexo') ? old('sexo') : $mascota->sexo;
$edad= old('edad') ? old('edad') : $mascota->edad;
$fecha= old('fecha') ? old('fecha') : $mascota->fechaIngreso;
$estado= old('estado') ? old('estado') : $mascota->estado;    
$raza= old('raza') ? old('raza') : $mascota->raza;
$condicion= old('condicion') ? old('condicion') : $mascota->condicion;
$est= old('est') ? old('est') : $mascota->esterelizado;
$especie= old('especie') ? old('especie') : $mascota->especie;    

?>

<div class="jumbotron text-center" id="cajaUsario">
  <div class="text-center">
    <h1>Nombre: {{ $nombre }} </h1>
  </div>
</div>
<div class="row">
  <div class="col-md-4 text-center">

    <img src="images/{{$foto}}" id="avatar" style=" width: 100%;">

  </div>
  <div class="col-sm-4">
   <h3>Datos de la mascota </h3>
   <hr>
   <ul class="" style="ist-style:none;">
     <li><b>Nombre :</b> {{ $nombre }} </li>
     <li><b>Edad :</b> {{ $edad }} </li>
     <li><b>Fecha Rescate :</b> {{ $fecha }} </li>
     <li><b>Sexo :</b>
      <?php if ($sexo == 1): ?>
        Macho
       <?php else: ?>
         Hembra
       <?php endif ?> 
     </li>


   </ul>
 </div>
 <div class="col-sm-4">

   <h3>Estado de Mascota</h3>
   <hr>
   <ul>
     <li><b>Estado :</b> 
      @foreach($categorias as $categoria)
      <?php if ($categoria->idCat == $estado): ?>
        {{ $categoria->nombre }}
      <?php endif ?>
      @endforeach 
    </li>
    <li><b>Raza :</b> {{ $raza }} </li>
    <li><b>Especie :</b> 
      @foreach($categorias as $categoria)
      <?php if ($categoria->idCat == $especie): ?>
        {{ $categoria->nombre }}
      <?php endif ?>
      @endforeach 
    </li>
    <li><b>Esterelizad@:</b> 
      <?php if ($est == 1): ?>
       Si
       <?php else: ?>
         No
         <?php endif ?> </li>
       </ul>
       <p><b>Condicion Medica:</b> {{ $condicion }}</p>

     </div>
     
     <div class="col-md-12 " >
      <center>
       <h3>Opciones</h3>
       <hr>
       <a href="#" class="btn btn-sm btn-success opciones" data-id="{{$mascota->idMascota}}" data-toggle="modal" data-target="#modalForm">Adoptar/Apadrinar</a>       
       @if (Auth::check())
       <a href="findMascota_{{$mascota->idMascota}}" title="Editar" class="btn btn-sm btn-info"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a>
       <a href="deleteMascota_{{$mascota->idMascota}}" title="Eliminar"  class="btn btn-sm btn-danger delete"><i class="fa fa-trash" aria-hidden="true"></i> Eliminar</a>
       @endif
       <a href="{{ URL::previous() }}" title="Regresar"  class="btn btn-sm btn-secondary"><i class="fa fa-trash" aria-hidden="true"></i> CANCELAR</a>
     </center>
   </div>
 </div>
 <hr>  
 <br>
</div>
<div class="modal fade" id="modalForm" role="dialog">
    <div class="modal-dialog">
      <form action="gestionarMascota" method="POST">
        {{ csrf_field() }}
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Gestion de Mascota</h4>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                <p class="statusMsg"></p>
                <img src="images/{{$foto}}" id="avatar" style=" width: 100%;">
                <div class ="col-md-12 row">
                    <div class ="col-md-4 bg-primary text-white">Nombre</div>
                    <div class ="col-md-4 bg-success text-white">Edad</div>
                    <div class ="col-md-4 bg-danger text-white">Sexo</div>
                </div>
                <div class ="col-md-12 row" >
                    <div class ="col-md-4 bg-primary text-white" >{{$nombre}}</div>
                    <div class ="col-md-4 bg-success text-white">{{$edad}}</div>
                    <div class ="col-md-4 bg-danger text-white">
                         <?php if ($sexo == 1): ?>
                        Macho
                       <?php else: ?>
                         Hembra
                       <?php endif ?> 
                    </div>
                </div>
                
                <form role="form">
                 <p class="p-2">Seleccione la opcion que desea realizar</p>
                  <input type="hidden" name="idMascota" value="{{$mascota->idMascota}}">
                  <input type="hidden" name="idEntidad" value="13">
                    <div class="form-group">
                        <select class="form-control" id="opcion" name="opcion" required="">
                          <option>Escoja una accion</option>
                          <option value="7">Adoptar</option>
                          <option value="8">Apadrinar</option>
                        </select>
                    </div>
                    <p>
                       * Nos pondremos en contacto para tramitar adopcion 
                    </p>                    
                </form>
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" name="submit" value="AGREGAR" class="btn btn-success">
            </div>
        </div>
        </form>
    </div>
</div>
<script>
function submitContactForm(){

    /*var reg = /^[A-Z0-9._%+-]+@([A-Z0-9-]+.)+[A-Z]{2,4}$/i;
=======
    var reg = /^[A-Z0-9._%-]@([A-Z0-9-].)[A-Z]{2,4}$/i;
>>>>>>> ea03397d180be365f0a994e7f0b2fbe05247bf28
    var name = $('#inputName').val();
    var email = $('#inputEmail').val();
    var message = $('#inputMessage').val();
    if(name.trim() == '' ){
        alert('Please enter your name.');
        $('#inputName').focus();
        return false;
    }else if(email.trim() == '' ){
        alert('Please enter your email.');
        $('#inputEmail').focus();
        return false;
    }else if(email.trim() != '' && !reg.test(email)){
        alert('Please enter valid email.');
        $('#inputEmail').focus();
        return false;
    }else if(message.trim() == '' ){
        alert('Please enter your message.');
        $('#inputMessage').focus();
        return false;
    }else{*/
      alert('Hola mundo'):
        $.ajax({
            var id = $(this).attr("data-id");
            type:'POST',
            url:'gestionarMascota',            
            url:'submit_form.php',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data:{id_Mascotaa:id},

            beforeSend: function () {
                $('.submitBtn').attr("disabled","disabled");
                $('.modal-body').css('opacity', '.5');
            },
            success:function(msg){
                if(msg == 'ok'){
                    $('#opciones .opcion').attr("value",data.opcion);
                   
                    $('.statusMsg').html('<span style="color:green;">Thanks for contacting us, well get back to you soon.</p>');
                }else{
                    $('.statusMsg').html('<span style="color:red;">Some problem occurred, please try again.</span>');
                }
                $('.submitBtn').removeAttr("disabled");
                $('.modal-body').css('opacity', '');
            }
        });
    
}
</script>
<?php
include "footer.php";
?>
