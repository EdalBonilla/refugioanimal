<!DOCTYPE html>
<html>
<head>
	<title>Control de gastos</title>

<!-- Bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Bootstrap -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!--JQuery -->
<script  src="https://code.jquery.com/jquery-1.12.4.js"></script>

<!--Data Tables -->
<script  src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<!--Data Tables -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">

@stack('styles')
</head>
<body>
<body>

	<nav class="navbar navbar-inverse">
  		<ul class="nav navbar-nav">
        	<li><a href="{{ url('categorias') }}">Categorías</a></li>
        	<li><a href="{{ url('conceptos')}} ">Conceptos</a></li>
    	</ul>
	</nav>

        @yield('content')
    </body>
</body>
</html>
