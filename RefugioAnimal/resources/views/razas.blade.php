<?php
include 'menu.php';
?>

<div class="jumbotron text-center" id="cajaUsario">
	<div class="text-center">
		<h1>Listado de razas </h1>  
	</div>
	
</div>
<a href="createRaza" title="new" class="btn btn-success"><i class="fa fa-pencil" aria-hidden="true"></i> Registrar Nuevo</a>
<table id="myTable" class="table table-striped  dt-responsive nowrap" cellspacing="0" width="100%">
	<thead>
    <tr>
      <th>#</th>
      <th>NOMBRE</th>
      <th>DESCRIPCIÓN</th>
      <th>OPCIONES</th>
    </tr>
	</thead>
	<tbody>
	<?php $counter=1;  ?>
 	@foreach($lista as $raza)
    	<tr>
    		<td>
    			{{ $counter }}
    		</td>
		    <td>
		    	{{ $raza->nombre }}
		    </td>
		    <td>
		    	{{ $raza->descripcion }}
		    </td>
		   
		    <td>
		    	<a href="findRaza_{{$raza->idRaza}}" title="Editar" class="btn btn-sm btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i> EDITAR</a>
		    	<a href="deleteRaza_{{$raza->idRaza}}" title="Eliminar"  class="btn btn-sm btn-danger delete"><i class="fa fa-trash" aria-hidden="true"></i> ELIMINAR</a>
		    </td>
	    </tr>
	    <?php $counter++; ?>
	@endforeach
</tbody>
</table>
<?php
include "footer.php";
?>