<?php
include 'menu.php';
?>
<div class="jumbotron text-center" id="cajaUsario">
	<div class="text-center">
		<h1>Listado de donacines </h1>  
	</div>
	
</div>
<div >
	<table id="myTable" class="table table-striped  dt-responsive nowrap" cellspacing="0" width="100%">
		<thead class="thead-dark">
			<tr>
				<th>#</th>
				<th>nombre</th>
				<th>Categoria</th>
				<th>Forma de pago</th>
				<th>Periodo de pago</th>
				<!--<th>Opciones</th>-->
			</tr>
		</thead>
		<tbody>
			<?php $counter = 1;  ?>
			@foreach ($lista as $item)
			<tr>
				<td>{{ $counter  }}</td>
				<td>{{ $item->entidad->nombre }}</td>
				<td>{{ $item->categoria->nombre }}</td>
				<td>{{ $item->formaDePago->nombre }}</td>
				<td>{{ $item->periodoPago }}</td>
				<!--	<td>
		   		<a href="findEntidad_{{$item->idEntidad}}" title="Editar" class="btn btn-sm btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i> EDITAR</a>
		    	<a href="deleteEntidad_{{$item->idEntidad }}" title="Eliminar"  class="btn btn-sm btn-danger delete">
		    	<i class="fa fa-trash" aria-hidden="true">
		    	</i> ELIMINAR
		    	</a>
		   			 </td>-->

			</tr>
			<tr>
				<td colspan="5">
					<div>
						<h4>Detalles de donación</h4>
						<ul>
							@if($item->detalle !=null)
								@foreach ($item->detalle as $itemDetallado)
								<li>{{$itemDetallado->descripcion}}, Cantidad:{{$itemDetallado->cantidad}},
								    @if($itemDetallado->tipo !=null)
								        Tipo:{{$itemDetallado->tipo->nombre}}
								    @endif
								</li>
								@endforeach
							@endif
						</ul>
					</div>
				</td>
			</tr>
			<?php $counter++; ?>
			@endforeach
		</tbody>
	</table>

</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#entidades_table').DataTable();
	});
</script>
<?php
include "footer.php";
?>
