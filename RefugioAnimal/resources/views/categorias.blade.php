<?php
include 'menu.php';
?>
<div class="jumbotron text-center" id="cajaUsario">
	<div class="text-center">
		<h1>Listado de categorias </h1>  
	</div>
	
</div>
<a href="createCategoria" title="new" class="btn btn-sm btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i> New</a>
<table id="myTable" class="table table-striped  dt-responsive nowrap" cellspacing="0" width="100%">
	<thead>
    <tr>
      <th>#</th>
      <th>NOMBRE</th>
      <th>DESCRIPCIÓN</th>
      <th>TIPO</th>
    </tr>
	</thead>
	<tbody>
	<?php $counter=1;  ?>
 	@foreach($lista as $categoria)
    	<tr>
    		<td>
    			{{ $counter }}
    		</td>
		    <td>
		    	{{ $categoria->nombre }}
		    </td>
		    <td>
		    	{{ $categoria->descripcion }}
		    </td>
		    
		    <td>
		    	{{ $categoria->tipo }}
		    </td>
		   
		    <td>
		    	<a href="findCategoria_{{$categoria->idCat}}" title="Editar" class="btn btn-sm btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i> EDITAR</a>
		    	<a href="deleteCategoria_{{$categoria->idCat}}" title="Eliminar"  class="btn btn-sm btn-danger delete"><i class="fa fa-trash" aria-hidden="true"></i> ELIMINAR</a>
		    </td>
	    </tr>
	    <?php $counter++; ?>
	@endforeach
</tbody>
</table>
<?php
include "footer.php";
?>