<?php
	$idEntidad = $entidades->idEntidad;
	$nombre = old('nombre')?old('nombre') : $entidades->nombre;
	$email = old('email')?old('email') : $entidades->email;
	$telefono = old('telefono')?old('telefono') : $entidades->telefono;
	$categoria = old('categoria')?old('categoria') : $entidades->categoria;	
	$estado = old('estado')?old('estado') : $entidades->estado;

?>
<form action="updateEntidad" method="POST">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
	  	<input type="hidden" id="id" name="_field" value="{{ csrf_field() }}">
	  		<input type="hidden" name="id" value="{{ $idEntidad }}">


<div class="container">
	  <div class="form-group ">


    <label for="nombre">Nombre</label>
	    <div class="col-md-8">
					<input type="text" class="form-control" id="name" name="nombre" placeholder="Ingrese el nombre del área" value="{{ $nombre }}">
					@if($errors->has('nombre'))
						<span class="label label-warning">{{$errors->first('nombre')}}</span>
					@endif 	
				</div>
	  </div>

<div class="form-group">
	    <label for="email">email</label>
	    <input type="text" class="form-control" id="email" name="email" 
	    placeholder="email" value="{{ $entidades->email }}">
	  </div>

<div class="form-group">
	    <label for="cel">telefono</label>
	    <input type="text" class="form-control" id="telefono" name="telefono" 
	    placeholder="telefono" value="{{ $entidades->telefono }}">
	  </div>	  

<center>
        		<br>
				<input type="submit" class="btn btn-sm btn-primary" name="submit" value="GUARDAR">
				<input type="reset" class="btn btn-sm btn-default" value="CANCELAR">
			</center>
  	</div>
</form>