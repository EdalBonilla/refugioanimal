<?php
include 'menu.php';
?>

<?php 
		$nombre= old('nombre') ? old('nombre') : $donable->nombre;
		$descripcion= old('descripcion') ? old('descripcion') : $donable->descripcion;
		$categoria= old('categoria') ? old('categoria') : $donable->idCategoria;			
?>

<div class="page-header">
	<h1>Donables</h1>
</div>

<div class="container">
	<form action="/donables/modificar" method="POST">
	<input type="hidden" name="donable" value="{{$donable->idDonable}}">
		{{ csrf_field() }}
		<div class="panel">
			<div class="panel-content">
				<div>
					<h4>Nombre</h4>
					<input type="text" name="nombre" value="{{$nombre}}">
					@if($errors->has('nombre'))
						<span class="label label-warning">{{$errors->first('nombre')}}</span>
					@endif 
				</div>
				<div>
					<h4>Descripción</h4>
					<textarea name="descripcion" cols="30" rows="10">{{$descripcion}}</textarea>
					@if($errors->has('descripcion'))
						<span class="label label-warning">{{$errors->first('descripcion')}}</span>
					@endif 
				</div>
				<select name="categoria" id="categoria">
					@foreach($categorias as $category)
						<option value="{{$category->idCat}}" 
							@if($category->idCat==$categoria)
								selected 
							@endif
							>{{$category->nombre}}</option>
					@endforeach
				</select>
				@if($errors->has('categoria'))
						<span class="label label-warning">{{$errors->first('categoria')}}</span>
					@endif 

				<input type="submit" value="Hacer Donativo">
				<input type="submit" value="Hacer Donativo">
			</div>
		</div>
	</form>
</div>
<?php
include "footer.php";
?>
