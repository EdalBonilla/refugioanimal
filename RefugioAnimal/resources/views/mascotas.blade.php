<?php
include 'menu.php';
?>

<div class="jumbotron text-center" id="cajaUsario">
	<div class="text-center">
		<h1>Listado de cachorros </h1>  
	</div>
	
</div>
<a href="createMascota" title="new" class="btn btn-success"><i class="fa fa-pencil" aria-hidden="true"></i> Registrar Nuevo</a>
<table id="myTable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="90%">
	<thead>
		<tr>
			<th>#</th>
			<th>NOMBRE</th>
			<th>SEXO</th>
			<th>FECHA INGRESO</th>
			<th>RAZA</th>
			<th>OPCIONES</th>
			
		</tr>
	</thead>
	<tbody>
		<?php $counter=1;  ?>
		@foreach($lista as $mascota)
		<tr>
			<td>
				{{ $counter }}
			</td>
			<td>
				{{ $mascota->nombre }}
			</td>
			<td>
				
				<?php if ( $mascota->sexo == 1): ?>
        Macho
       <?php else: ?>
         Hembra
       <?php endif ?>
			</td>
			<td>
				{{ $mascota->fechaIngreso }}
			</td>

			<td>
				{{ $mascota->raza }}
			</td>		    

			<td>
				<a href="adminMascota_{{$mascota->idMascota}}" title="Administrar" class="btn btn-sm btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i>VER</a>
				<a href="findMascota_{{$mascota->idMascota}}" title="Editar" class="btn btn-sm btn-info"><i class="fa fa-pencil" aria-hidden="true"></i> EDITAR</a>
				<a href="deleteMascota_{{$mascota->idMascota}}" title="Eliminar"  class="btn btn-sm btn-danger delete"><i class="fa fa-trash" aria-hidden="true"></i> ELIMINAR</a>
			</td>
		</tr>
		<?php $counter++; ?>
		@endforeach
	</tbody>
</table>
<?php
include "footer.php";
?>
