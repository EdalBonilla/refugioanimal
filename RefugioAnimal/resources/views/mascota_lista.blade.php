<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="./css/inicio.css">
  <link rel="stylesheet" href="../css/pets.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Knewave&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Signika&display=swap" rel="stylesheet">

  <title>Salvadog!</title>

</head>

<body style="background-color: #ffff; ">
  <script type="text/javascript">
    // Select all links with hashes
    $('a[href*="#"]')
      // Remove links that don't actually link to anything
      .not('[href="#0"]')
      .click(function(event) {
        // On-page links
        if (
          location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
          location.hostname == this.hostname
        ) {
          // Figure out element to scroll to
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          // Does a scroll target exist?
          if (target.length) {
            // Only prevent default if animation is actually gonna happen
            event.preventDefault();
            $('html, body').animate({
              scrollTop: target.offset().top
            }, 500, function() {
              // Callback after animation
              // Must change focus!
              var $target = $(target);
              $target.focus();
              if ($target.is(":focus")) { // Checking if the target was focused
                return false;
              } else {
                $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                $target.focus(); // Set focus again
              };
            });
          }
        }
      });
  </script>

<nav id="menu" class="navbar fixed-top navbar-expand-lg na   navbar-dark  " >
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="nav navbar-nav ">
      <li class="nav-item active">
        <a class="nav-link" href="/">Salvadog <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href  ="#fondo3">Acerca de</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#fondo4">Servicios</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#fondo5">Nuestra familia</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#fondo-footer">Contacto</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="mascota_lista">Apadrina un cachorro</a>
      </li>
@if (Auth::check())
    <li class="nav-item">
        <a class="nav-link" href="adminEntidad">Administrar</a>
      </li>
    <a href="logout">
    <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Cerrar Sesión</button></a>
@else
    <a href="login">
    <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Iniciar Sesión</button></a>
@endif
    </ul>

  </div>
  
  </nav>


  <div class="fondo" id="fondo1_pet">

    <div class="row" style="margin-right: 0; margin-left: 0;">
      <div class="col-xs-12 col-sm-7">
        <h2 style="" id="htitulo">Es tiempo de darle amor a uno mas</h2>
      </div>

    </div>



  </div>

  <div  id="caja0_pet">
    <div class="container">
      <div class="row" style="margin-right: 0; margin-left: 0; text-align: center;">
        <div class="caja-title col-md-12 mb-3" style="margin-bottom: 4rem!important;">
          <h2 class="title2">Huellitas Disponibles</h2>
          <hr>
          <div class="container" style="width: 78%;">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
            labore et dolore magna aliqua, Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
            labore et dolore magna aliqua
          </div>
        </div>
        @foreach ($lista as $mascota)
          <div class="col-md-4 mb-3" >
            <figure class="snip1361">
              <img src="images/{{$mascota->foto}}" alt="sample45" />
              <figcaption>
                <h3>{{ $mascota->nombre }}</h3>
                <a href="adminMascota_{{$mascota->idMascota}}" title="Ver mascota" class="btn btn-sm" ><i class="fa fa-pencil" aria-hidden="true"></i>VER</a>
              </figcaption>
            </figure>
          </div>
       @endforeach

      </div>
    </div>
  </div>

  <div id="fondo-footer" >
      <h2 class="title2" >Ponte en contacto</h2>
      <hr/>
      <!-- Footer -->
      <footer class="page-footer font-small teal pt-4">

        <!-- Footer Text -->
        <div class="container-fluid text-center text-md-left">

          <!-- Grid row -->
          <div class="row texfooter" >

            <!-- Grid column -->
            <div class="col-md-6 mt-md-0 mt-3">

              <!-- Content -->
              <h5 class="text-uppercase font-weight-bold title5">Dirección</h5>

              <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Optio deserunt fuga perferendis modi earum
                commodi aperiam temporibus quod nulla nesciunt aliquid debitis ullam omnis quos ipsam, aspernatur id
              excepturi hic.</p>

            </div>
            <!-- Grid column -->

            <hr class="clearfix w-100 d-md-none pb-3">

            <!-- Grid column -->
            <div class="col-md-6 mb-md-0 mb-3">

              <!-- Content -->

              <h5 class="text-uppercase font-weight-bold title5">Correo electrónico</h5>
              <p class="texfooder">Lorem ipsum, dolor sit amet consectetur adipisicing elit.</p>
              <h5 class="text-uppercase font-weight-bold title5">Teléfonos</h5>
              <p class="texfooder">+503 2222-2222 / +503 7777-7777.</p>



            </div>
            <!-- Grid column -->

          </div>
          <!-- Grid row -->

        </div>
        <!-- Footer Text -->
        <hr/>
        <!-- Copyright -->
        <div class="footer-copyright text-center py-3">© 2019 Copyright:
          <a href="#"> Salvadog.com</a>
        </div>
        <!-- Copyright -->

      </footer>
      <!-- Footer -->
    </div>
  </div>


  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script type="text/javascript">
    $(window).scroll(function() {
      if ($("#menu").offset().top > 56) {
        00
        $("#menu").addClass("bg-dark");
      } else {
        $("#menu").removeClass("bg-dark");
      }
    });
  </script>
</body>

</html>
