<?php
include 'menu.php';
?>

<div class="jumbotron text-center" id="cajaUsario">
	<div class="text-center">
		<h1>Listado de Usuarios </h1>  
	</div>
	
</div>
<a href="createEntidad" title="new" class="btn btn-success"><i class="fa fa-pencil" aria-hidden="true"></i> Registrar Nuevo</a>
	<table class="table" id="categorias_table">
		<thead class="thead-dark">
			<tr>
				<th>#</th>
				<th>Titulo</th>
				<th>Cantidad</th>
				<th>Descripcion</th>
				<th>Insumo</th>
				<th>Opciones</th>
			</tr>
		</thead>
		<tbody>
			<?php $counter = 1;  ?>
			@foreach ($lista as $item)
			<tr>
				<td>{{ $counter  }}</td>
				<td>{{ $item->titulo }}</td>
				<td>{{ $item->cantidad }}</td>
				<td>{{ $item->descripcion }}</td>
				<td>{{ $item->idInsumo }}</td>
					<td>
		   		<a href="necesidades/necesidad/{{$item->idNecesidad}}" title="Donar" class="btn btn-sm btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i> Donar</a>
		    	<!--<a href="deleteEntidad_{{$item->idNecesidad }}" title="Eliminar"  class="btn btn-sm btn-danger delete">
		    	<i class="fa fa-trash" aria-hidden="true">
		    	</i> ELIMINAR
		    	</a>-->
		   			 </td>

			</tr>
			<?php $counter++; ?>
			@endforeach
		</tbody>
	</table>

</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#entidades_table').DataTable();
	});
</script>
<?php
include "footer.php";
?>
