<?php
include 'menu.php';
?>

<div class="page-header">
	<h1>Items donables</h1>
</div>

<div class="container">
	<form action="/donables/crear" method="POST">
		{{ csrf_field() }}
		<div class="panel">
			<div class="panel-content">
				<div>
					<h4>Nombre</h4>
					<input name="nombre" type="text">
					@if($errors->has('nombre'))
						<span class="label label-warning">{{$errors->first('nombre')}}</span>
					@endif 
				</div>
				<div>
					<h4>Descripción</h4>
					<input name="descripcion" type="text" />
					@if($errors->has('descripcion'))
						<span class="label label-warning">{{$errors->first('descripcion')}}</span>
					@endif 
				</div>
				<div>
					<h4>Categoria</h4>
					<select name="categoria" id="categoria">
						@foreach($categorias as $categoria)
						<option value="{{$categoria->idCat}}">{{$categoria->nombre}}</option>	
						@endforeach
					</select>
					@if($errors->has('categoria'))
						<span class="label label-warning">{{$errors->first('categoria')}}</span>
					@endif 
				</div>
				<input type="submit" value="Guardar">
			</div>
		</div>
	</form>
</div>
<?php
include "footer.php";
?>
