<html>
    <head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="../css/inicio.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Knewave&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Signika&display=swap" rel="stylesheet">
  <title>Salvadog</title>

</head>
<body style="background-image: url('../img/fondo_login1.jpg'); background-repeat: no repeat; background-size:cover;">
   <nav id="menu" class="navbar fixed-top navbar-expand-lg na   navbar-dark  " >
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="nav navbar-nav ">
      <li class="nav-item active">
        <a class="nav-link" href="/">Salvadog <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href  ="#fondo3">Acerca de</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#fondo4">Servicios</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#fondo5">Nuestra familia</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#fondo-footer">Contacto</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="mascota_lista">Apadrina un cachorro</a>
      </li>
@if (Auth::check())
    <li class="nav-item">
        <a class="nav-link" href="adminEntidad">Administrar</a>
      </li>
    <a href="logout">
    <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Cerrar Sesión</button></a>
@else
    <a href="login">
    <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Iniciar Sesión</button></a>
@endif
    </ul>

  </div>
  
  </nav>
  
<div class="container" id="cajalogin">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>
                                 <button type="submit" class="btn btn-info"><a href="register" style="color:white;">Register</a>
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>    
</body>

</html>
