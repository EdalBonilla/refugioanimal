<?php
include 'menu.php';
?>
<div class="row" style="    flex-wrap: wrap;
margin-right: 0;
margin-left: 0">
<div class="col-md-4 text-center" id="cajadd1">

	<img src="/images/user.png" id="avatar">

</div>
<div class="col-md-7 " id="cajadd2">
	<h5 class="text-center" id="tituloadd">Ingreso del nuevo usuario</h5>
	<hr>
	<form action="entidad_add" method="POST" class="form-validation" enctype="multipart/form-data">
{{ csrf_field() }}

		<div class="container">
			<div class="form-group ">
				<label for="nombre">Nombre</label>
				<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Compras, salidas, etc" value="{{ old('nombre') }}">
				@if($errors->has('nombre'))
				<span class="label label-warning">{{$errors->first('nombre')}}</span>
				@endif
			</div>
			<div class="form-group">
				<label for="email">Correo</label>
				<input type="text" class="form-control" id="email" name="email" 
				placeholder="Descripción" value="{{ old('email') }}">
			</div>
			<div class="form-group">
				<label for="telefono">cel</label>
				<input type="text" class="form-control" id="telefono" name="telefono" 
				placeholder="Descripción" value="{{ old('telefono') }}">
			</div>
			<div class="form-group">
				<label for="categoria">categoria</label>
				<input type="number" class="form-control" id="categoria" name="categoria" 
				placeholder="Descripción" value="{{ old('categoria') }}">
			</div>
			<div class="form-group">
				<label for="password">password</label>
				<input type="text" class="form-control" id="password" name="password" 
				placeholder="Descripción" value="{{ old('password') }}">
			</div>
			<div class="form-group">
				<label for="estado">estado</label>
				<input type="number" class="form-control" id="estado" name="estado" 
				placeholder="Descripción" value="{{ old('estado') }}">
			</div>




			<div class="form-group">
				<input type="submit" name="submit" value="AGREGAR" class="btn btn-success">
			</div>
		</div>
	</form>
	<script>
      // Example starter JavaScript for disabling form submissions if there are invalid fields
      (function() {
      	'use strict';
      	window.addEventListener('load', function() {
          // Fetch all the forms we want to apply custom Bootstrap validation styles to
          var forms = document.getElementsByClassName('needs-validation');
          // Loop over them and prevent submission
          var validation = Array.prototype.filter.call(forms, function(form) {
          	form.addEventListener('submit', function(event) {
          		if (form.checkValidity() === false) {
          			event.preventDefault();
          			event.stopPropagation();
          		}
          		form.classList.add('was-validated');
          	}, false);
          });
      }, false);
      })();
  </script>
</div>
</div>



<?php
include "footer.php";
?>
