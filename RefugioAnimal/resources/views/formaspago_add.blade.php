<?php
include 'menu.php';
?>
<div class="page-header">
	<h1>Donaciones ¡Formas de pago seran pre-establecidas!</h1>
</div>

<div class="container">
	<form action="/pagos/guardar" method="POST">
		{{ csrf_field() }}
		<div class="panel">
			<div class="panel-content">
				<div>
					<h4>Nombre</h4>
					<input name="nombre" type="text">
				</div>
				<div>
					<h4>Icono</h4>
					<input name="icono" type="text" />
				</div>
				<div>
					<h4>Estado del medio de pago</h4>
					<select name="estado" id="estado">
						<option value="0">Inactivo</option>
						<option value="1">Activo</option>
					</select>
				</div>
				<div>
					<h4>Otros detalles</h4>
					<textarea name="otros_detalles" cols="30" rows="10"></textarea>
				</div>

				<input type="submit" value="Guardar">
			</div>
		</div>
	</form>
</div>
<?php
include "footer.php";
?>
