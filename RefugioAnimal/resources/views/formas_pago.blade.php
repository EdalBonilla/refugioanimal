<?php
include 'menu.php';
?>

<div class="page-header">
	<h1>Formas de Pago</h1>
</div>

<div class="container">
	<table class="table" id="categorias_table">
		<thead class="thead-dark">
			<tr>
				<th>#</th>
				<th>Nombre</th>
				<th>Estado</th>
				<th>Otros Detalles</th>
				
				<!--<th>Opciones</th>-->
			</tr>
		</thead>
		<tbody>
			<?php $counter = 1;  ?>
			@foreach ($lista as $item)
			<tr>
				<td>{{ $counter  }}</td>
				<td>{{ $item->nombre }}</td>
				<td>{{ $item->estado }}</td>
				<td>{{ $item->otros_detalles }}</td>
				
					<!--<td>
		   		<a href="necesidades/{{$item->idNecesidad}}" title="Donar" class="btn btn-sm btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i> Donar</a>
		    	<a href="deleteEntidad_{{$item->idEntidad }}" title="Eliminar"  class="btn btn-sm btn-danger delete">
		    	<i class="fa fa-trash" aria-hidden="true">
		    	</i> ELIMINAR
		    	</a>
		   			 </td>-->
			</tr>
			<?php $counter++; ?>
			@endforeach
		</tbody>
	</table>

</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#entidades_table').DataTable();
	});
</script>
<?php
include "footer.php";
?>
